Version 4
SHEET 1 1944 964
WIRE 384 -224 320 -224
WIRE 496 -224 448 -224
WIRE 496 -160 496 -224
WIRE 528 -160 496 -160
WIRE 752 -160 752 -224
WIRE 752 -160 720 -160
WIRE 864 -144 768 -144
WIRE -96 -80 -96 -96
WIRE 64 -80 64 -96
WIRE 288 -64 256 -64
WIRE 528 -64 368 -64
WIRE 768 -64 768 -144
WIRE 768 -64 720 -64
WIRE 816 -64 768 -64
WIRE 928 -64 896 -64
WIRE 256 -48 256 -64
WIRE 928 -48 928 -64
WIRE 1280 176 1280 112
WIRE 336 224 336 208
WIRE 336 224 304 224
WIRE 368 224 336 224
WIRE 528 224 448 224
WIRE 1280 272 1280 256
WIRE 528 320 480 320
WIRE 816 320 720 320
WIRE 960 320 896 320
WIRE 1104 320 1024 320
WIRE 1216 320 1184 320
WIRE 480 336 480 320
WIRE 1632 384 1632 80
WIRE 1632 384 1472 384
WIRE 1280 432 1280 368
WIRE 1328 432 1280 432
WIRE 1408 432 1328 432
WIRE 1632 432 1632 384
WIRE 1504 480 1472 480
WIRE 1568 480 1504 480
WIRE 1328 592 1328 432
WIRE 1376 592 1328 592
WIRE 1504 592 1504 480
WIRE 1504 592 1456 592
WIRE 1536 592 1504 592
WIRE 1632 592 1632 528
WIRE 1632 592 1616 592
WIRE 1632 640 1632 592
FLAG -96 -80 0
FLAG 64 -80 0
FLAG 64 -176 3.3V
FLAG -96 -176 12V
FLAG 256 -48 0
FLAG 928 -48 0
FLAG 752 -224 3.3V
FLAG 240 -224 12V
FLAG 1632 0 12V
FLAG 1632 640 0
FLAG 720 224 12V
FLAG 480 336 0
FLAG 336 208 3.3V
FLAG 1280 112 12V
FLAG 864 -144 Out1
IOPIN 864 -144 Out
FLAG 304 224 In1
IOPIN 304 224 In
SYMBOL TLP291_4 624 -112 R0
SYMATTR InstName U1
SYMBOL voltage -96 -192 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V1
SYMATTR Value 12
SYMBOL voltage 64 -192 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V2
SYMATTR Value 3.3
SYMBOL res 336 -240 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R3
SYMATTR Value 2.2k
SYMBOL res 912 -80 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R4
SYMATTR Value 10k
SYMBOL LED 384 -208 R270
WINDOW 0 72 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName D1
SYMATTR Value LUW-W5AP
SYMATTR Description Diode
SYMATTR Type diode
SYMBOL npn 1408 384 R0
SYMATTR InstName Q3
SYMBOL res 1616 -16 R0
SYMATTR InstName R9
SYMATTR Value 8
SYMBOL npn 1568 432 R0
SYMATTR InstName Q4
SYMBOL res 1200 304 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R10
SYMATTR Value 2.5k
SYMBOL res 1632 576 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R11
SYMATTR Value 3k
SYMBOL res 1472 576 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R12
SYMATTR Value 7.2k
SYMBOL LED 960 336 R270
WINDOW 0 72 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName D2
SYMATTR Value LUW-W5AP
SYMATTR Description Diode
SYMATTR Type diode
SYMBOL res 912 304 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R13
SYMATTR Value 470
SYMBOL res 464 208 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R14
SYMATTR Value 470
SYMBOL npn 1216 272 R0
SYMATTR InstName Q5
SYMBOL res 1264 160 R0
SYMATTR InstName R15
SYMATTR Value 900
SYMBOL csw 288 -64 R270
WINDOW 0 32 40 VTop 2
WINDOW 3 -32 40 VBottom 2
SYMATTR InstName SW1
SYMATTR Value ""
TEXT -80 352 Left 2 !.tran 1
TEXT 1096 672 Left 2 ;ULN2068B
TEXT 368 -88 Left 2 ;Puerta
TEXT 1480 48 Left 2 ;Cerradura
TEXT 176 0 Left 2 ;Sensado de puertas
TEXT 288 736 Left 2 ;Activación Cerraduras
LINE Normal 1040 176 288 176 2
LINE Normal 1728 720 288 720 2
LINE Normal 1728 -80 1040 -80 2
LINE Normal 1040 176 1040 -80 2
LINE Normal 1728 720 1728 -80 2
LINE Normal 224 176 288 176 2
LINE Normal 224 720 224 176 2
LINE Normal 288 720 224 720 2
RECTANGLE Normal 1696 688 1088 128 2
RECTANGLE Normal 960 -16 176 -320 2
