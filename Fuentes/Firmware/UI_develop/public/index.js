const available_btn = document.querySelector('#disponibles');

//configure logger
const context = {context: "index.js"};

//send to main
function seeAvailable(){
    app_logger.info(context, `Cambiando a pantalla de casilleros disponibles`);
    ipcRenderer.send('page:available', null);
}

//event listeners
available_btn.addEventListener('click', seeAvailable);