/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./public/**/*.{html,js}"
  ],
  theme: {
    extend: {
      colors: {
        'galicia1': {
          '900': '#4f68f9',
          '200': '#b2bdfe',
        },
        'galicia2': {
          '900': '#cb28ff',
          '200': '#ecb2fe',
        }   
      },
      height: {
        '1/10': '10%',
        '8/10': '80%',
        '9/10': '90%',
        'lr1': '25.86%',
        'lr2': '24.195%',
        'lr3': '24.195%',
        'lr4': '25.75%',
      },
      keyframes: {
        pong: {
          '0%, 100%': {
            opacity: 1
          },
          '50%': {
            transform: 'scale(0.95)',
            opacity: 0.5
          }
        },
        wiggle: {
          '0%, 100%': { transform: 'rotate(-3deg)' },
          '50%': { transform: 'rotate(3deg)' },
        },
        bouncetop: {
          '0%': {
              transform: 'translateY(-450px)',
          },
          '24%': {
              opacity:1
          },
          '25%': {
            transform: 'translateY(0)',
          },
          '40%': {
              transform: 'translateY(-60px)',
          },
          '55%': {
            transform: 'translateY(0)',
          },
          '65%':{
              transform: 'translateY(-30px)',
          },
          '75%': {
            transform: 'translateY(0)',
          },
          '82%':{
              transform: 'translateY(-15px)',
          },
          '87%': {
            transform: 'translateY(0)',
          },
          '93%':{
              transform: 'translateY(-6px)',
          },
          '100%': {
              transform: 'translateY(0)',
          }
        },
      }, 
      animation: {
        pong: 'pong 3s cubic-bezier(0, 0, 0.2, 1) infinite',
        wiggle: 'wiggle 1s ease-in-out',
        bouncetop: 'bouncetop 1s linear',
      }
    },
  },
  plugins: [
    require("tw-elements/dist/plugin"),
  ],
  safelist:[
    'animate-[fade-in_2s_ease-in-out]',
    'animate-[fade-in-right_2s_ease-in-out]',
    'animate-[slide-in-left_2s_ease-in-out]',
  ]
}
