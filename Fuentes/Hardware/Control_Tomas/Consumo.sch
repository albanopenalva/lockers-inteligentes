EESchema Schematic File Version 4
LIBS:Control_Tomas-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ACS71020:ACS71020 U8
U 1 1 5DCC41FA
P 6200 3800
F 0 "U8" H 6200 4715 50  0000 C CNN
F 1 "ACS71020" H 6200 4624 50  0000 C CNN
F 2 "lib_fp:SOIC-16-W" H 6300 3700 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Allegro%20PDFs/ACS71020.pdf" H 6300 3700 50  0001 C CNN
F 4 "SINGLE PHASE, ISOLATED, POWER MO" H 6200 3800 50  0001 C CNN "Descripcion"
F 5 "Allegro MicroSystems" H 6200 3800 50  0001 C CNN "Fabricante"
F 6 "ACS71020KMABTR-090B3-I2C" H 6200 3800 50  0001 C CNN "Codigo Fabricante"
F 7 "620-1998-1-ND" H 6200 3800 50  0001 C CNN "Codigo Digikey"
	1    6200 3800
	1    0    0    -1  
$EndComp
Text HLabel 9375 3600 2    50   Input ~ 0
SDA
Text HLabel 9375 3700 2    50   Input ~ 0
SCL
Wire Wire Line
	6950 4400 7000 4400
Wire Wire Line
	7000 4400 7000 4450
$Comp
L power:+3.3V #PWR?
U 1 1 5DCC447A
P 7000 3150
AR Path="/5DCC447A" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCC447A" Ref="#PWR?"  Part="1" 
AR Path="/5DCC1C1C/5DCC447A" Ref="#PWR070"  Part="1" 
F 0 "#PWR070" H 7000 3000 50  0001 C CNN
F 1 "+3.3V" H 7015 3323 50  0000 C CNN
F 2 "" H 7000 3150 50  0001 C CNN
F 3 "" H 7000 3150 50  0001 C CNN
	1    7000 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DCC4480
P 7000 4450
AR Path="/5DCC4480" Ref="#PWR?"  Part="1" 
AR Path="/5DCC1C1C/5DCC4480" Ref="#PWR074"  Part="1" 
F 0 "#PWR074" H 7000 4200 50  0001 C CNN
F 1 "GND" H 7005 4277 50  0000 C CNN
F 2 "" H 7000 4450 50  0001 C CNN
F 3 "" H 7000 4450 50  0001 C CNN
	1    7000 4450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6950 3200 7000 3200
Wire Wire Line
	7000 3200 7000 3150
Text Notes 6800 3925 0    50   ~ 0
Address 1100000
Text HLabel 1950 2400 0    50   Input ~ 0
LINE_IN
Text HLabel 1950 2600 0    50   Input ~ 0
NEUT_IN
Text HLabel 1950 5350 0    50   Output ~ 0
LINE_OUT
Text HLabel 1950 5150 0    50   Output ~ 0
NEUT_OUT
Wire Wire Line
	3200 2400 3200 2900
Connection ~ 3200 3650
Connection ~ 3200 3550
Wire Wire Line
	3200 3550 3200 3650
Wire Wire Line
	3200 5350 1950 5350
Wire Wire Line
	1950 5150 3000 5150
Wire Wire Line
	6950 4100 7000 4100
Wire Wire Line
	7000 4100 7000 4150
$Comp
L power:GND #PWR?
U 1 1 5DCE29C6
P 7000 4150
AR Path="/5DCE29C6" Ref="#PWR?"  Part="1" 
AR Path="/5DCC1C1C/5DCE29C6" Ref="#PWR073"  Part="1" 
F 0 "#PWR073" H 7000 3900 50  0001 C CNN
F 1 "GND" H 7005 3977 50  0000 C CNN
F 2 "" H 7000 4150 50  0001 C CNN
F 3 "" H 7000 4150 50  0001 C CNN
	1    7000 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7000 4100 7000 4000
Wire Wire Line
	7000 4000 6950 4000
Connection ~ 7000 4100
Wire Wire Line
	3000 2600 3000 3300
Wire Wire Line
	3200 2400 1950 2400
Wire Wire Line
	3000 2600 1950 2600
Wire Wire Line
	3200 3650 3200 3750
Wire Wire Line
	3200 4100 3200 4200
Connection ~ 3200 3750
Wire Wire Line
	3200 3750 3200 3850
Connection ~ 3200 4200
Wire Wire Line
	3200 4200 3200 4300
Connection ~ 3200 4300
Wire Wire Line
	3200 4300 3200 4400
$Comp
L Device:C C2
U 1 1 5DCF42BC
P 4850 2900
F 0 "C2" V 4598 2900 50  0000 C CNN
F 1 "1uF" V 4689 2900 50  0000 C CNN
F 2 "lib_fp:C_0603" H 4888 2750 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0603C105K4RACTU.pdf" H 4850 2900 50  0001 C CNN
F 4 "CAP CER 1UF 16V X7R 0603" V 4850 2900 50  0001 C CNN "Descripcion"
F 5 "KEMET" V 4850 2900 50  0001 C CNN "Fabricante"
F 6 "C0603C105K4RACTU" V 4850 2900 50  0001 C CNN "Codigo Fabricante"
F 7 "399-7847-1-ND" V 4850 2900 50  0001 C CNN "Codigo Digikey"
	1    4850 2900
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5DCF439E
P 3750 2900
AR Path="/5DCF439E" Ref="R?"  Part="1" 
AR Path="/5DCC1C1C/5DCF439E" Ref="R37"  Part="1" 
F 0 "R37" V 3650 2900 50  0000 C CNN
F 1 "1M" V 3750 2900 50  0000 C CNN
F 2 "lib_fp:R_1206" V 3680 2900 50  0001 C CNN
F 3 "https://www.yageo.com/documents/recent/PYu-RV_51_RoHS_L_8.pdf" H 3750 2900 50  0001 C CNN
F 4 "RES SMD 1M OHM 1% 1/4W 1206" V 3750 2900 50  0001 C CNN "Descripcion"
F 5 "Yageo" V 3750 2900 50  0001 C CNN "Fabricante"
F 6 "RV1206FR-071ML" V 3750 2900 50  0001 C CNN "Codigo Fabricante"
F 7 "311-1MNICT-ND" V 3750 2900 50  0001 C CNN "Codigo Digikey"
	1    3750 2900
	0    1    -1   0   
$EndComp
Wire Wire Line
	3200 3850 5450 3850
Wire Wire Line
	3200 4100 5450 4100
Wire Wire Line
	3200 3650 5450 3650
Wire Wire Line
	3200 3550 5450 3550
Wire Wire Line
	3200 3750 5450 3750
Wire Wire Line
	3200 4200 5450 4200
Wire Wire Line
	3200 4300 5450 4300
Wire Wire Line
	3200 4400 5450 4400
Wire Wire Line
	5450 3200 5400 3200
Wire Wire Line
	5400 2900 5200 2900
Wire Wire Line
	4700 2900 4500 2900
Wire Wire Line
	4000 2900 3900 2900
Wire Wire Line
	3600 2900 3200 2900
Wire Wire Line
	4700 3300 4500 3300
Wire Wire Line
	4000 3300 3900 3300
Wire Wire Line
	3600 3300 3000 3300
Wire Wire Line
	5200 3250 5200 3300
Connection ~ 5200 3300
Wire Wire Line
	5200 3300 5000 3300
Wire Wire Line
	5200 2950 5200 2900
Connection ~ 5200 2900
Wire Wire Line
	5200 2900 5000 2900
Wire Wire Line
	4500 2950 4500 2900
Connection ~ 4500 2900
Wire Wire Line
	4500 2900 4300 2900
Wire Wire Line
	4500 3250 4500 3300
Connection ~ 4500 3300
Wire Wire Line
	4500 3300 4300 3300
$Comp
L power:GND #PWR?
U 1 1 5DD71DD8
P 7450 3250
AR Path="/5DD71DD8" Ref="#PWR?"  Part="1" 
AR Path="/5DCC1C1C/5DD71DD8" Ref="#PWR071"  Part="1" 
F 0 "#PWR071" H 7450 3000 50  0001 C CNN
F 1 "GND" H 7455 3077 50  0000 C CNN
F 2 "" H 7450 3250 50  0001 C CNN
F 3 "" H 7450 3250 50  0001 C CNN
	1    7450 3250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7100 3200 7000 3200
Connection ~ 7000 3200
Wire Wire Line
	7450 3250 7450 3200
Wire Wire Line
	7450 3200 7400 3200
Connection ~ 3000 3300
Connection ~ 3200 2900
Wire Wire Line
	3200 2900 3200 3550
Wire Wire Line
	3000 3300 3000 5150
Wire Wire Line
	5200 3300 5450 3300
Wire Wire Line
	5400 2900 5400 3200
$Comp
L power:GND #PWR?
U 1 1 5DD770AA
P 4500 3350
AR Path="/5DD770AA" Ref="#PWR?"  Part="1" 
AR Path="/5DCC1C1C/5DD770AA" Ref="#PWR072"  Part="1" 
F 0 "#PWR072" H 4500 3100 50  0001 C CNN
F 1 "GND" H 4505 3177 50  0000 C CNN
F 2 "" H 4500 3350 50  0001 C CNN
F 3 "" H 4500 3350 50  0001 C CNN
	1    4500 3350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4500 3300 4500 3350
Connection ~ 3200 4400
Wire Wire Line
	3200 4400 3200 5350
Wire Wire Line
	6950 3600 9375 3600
Wire Wire Line
	6950 3700 9375 3700
$Comp
L Device:R R?
U 1 1 5DDCDBC2
P 4150 2900
AR Path="/5DDCDBC2" Ref="R?"  Part="1" 
AR Path="/5DCC1C1C/5DDCDBC2" Ref="R38"  Part="1" 
F 0 "R38" V 4050 2900 50  0000 C CNN
F 1 "1M" V 4150 2900 50  0000 C CNN
F 2 "lib_fp:R_1206" V 4080 2900 50  0001 C CNN
F 3 "https://www.yageo.com/documents/recent/PYu-RV_51_RoHS_L_8.pdf" H 4150 2900 50  0001 C CNN
F 4 "RES SMD 1M OHM 1% 1/4W 1206" V 4150 2900 50  0001 C CNN "Descripcion"
F 5 "Yageo" V 4150 2900 50  0001 C CNN "Fabricante"
F 6 "RV1206FR-071ML" V 4150 2900 50  0001 C CNN "Codigo Fabricante"
F 7 "311-1MNICT-ND" V 4150 2900 50  0001 C CNN "Codigo Digikey"
	1    4150 2900
	0    1    -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5DDCE871
P 4150 3300
AR Path="/5DDCE871" Ref="R?"  Part="1" 
AR Path="/5DCC1C1C/5DDCE871" Ref="R42"  Part="1" 
F 0 "R42" V 4050 3300 50  0000 C CNN
F 1 "1M" V 4150 3300 50  0000 C CNN
F 2 "lib_fp:R_1206" V 4080 3300 50  0001 C CNN
F 3 "https://www.yageo.com/documents/recent/PYu-RV_51_RoHS_L_8.pdf" H 4150 3300 50  0001 C CNN
F 4 "RES SMD 1M OHM 1% 1/4W 1206" V 4150 3300 50  0001 C CNN "Descripcion"
F 5 "Yageo" V 4150 3300 50  0001 C CNN "Fabricante"
F 6 "RV1206FR-071ML" V 4150 3300 50  0001 C CNN "Codigo Fabricante"
F 7 "311-1MNICT-ND" V 4150 3300 50  0001 C CNN "Codigo Digikey"
	1    4150 3300
	0    1    -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5DDCF507
P 3750 3300
AR Path="/5DDCF507" Ref="R?"  Part="1" 
AR Path="/5DCC1C1C/5DDCF507" Ref="R41"  Part="1" 
F 0 "R41" V 3650 3300 50  0000 C CNN
F 1 "1M" V 3750 3300 50  0000 C CNN
F 2 "lib_fp:R_1206" V 3680 3300 50  0001 C CNN
F 3 "https://www.yageo.com/documents/recent/PYu-RV_51_RoHS_L_8.pdf" H 3750 3300 50  0001 C CNN
F 4 "RES SMD 1M OHM 1% 1/4W 1206" V 3750 3300 50  0001 C CNN "Descripcion"
F 5 "Yageo" V 3750 3300 50  0001 C CNN "Fabricante"
F 6 "RV1206FR-071ML" V 3750 3300 50  0001 C CNN "Codigo Fabricante"
F 7 "311-1MNICT-ND" V 3750 3300 50  0001 C CNN "Codigo Digikey"
	1    3750 3300
	0    1    -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5DDD03CC
P 4500 3100
AR Path="/5DDD03CC" Ref="R?"  Part="1" 
AR Path="/5DCC1C1C/5DDD03CC" Ref="R39"  Part="1" 
F 0 "R39" V 4400 3100 50  0000 C CNN
F 1 "2k2" V 4500 3100 50  0000 C CNN
F 2 "lib_fp:R_1206" V 4430 3100 50  0001 C CNN
F 3 "https://www.yageo.com/upload/media/product/productsearch/datasheet/rchip/PYu-RC_Group_51_RoHS_L_10.pdf" H 4500 3100 50  0001 C CNN
F 4 "RES SMD 1.8K OHM 5% 1/4W 1206" V 4500 3100 50  0001 C CNN "Descripcion"
F 5 "Yageo" V 4500 3100 50  0001 C CNN "Fabricante"
F 6 "RC1206JR-071K8L" V 4500 3100 50  0001 C CNN "Codigo Fabricante"
F 7 "311-1.8KERCT-ND" V 4500 3100 50  0001 C CNN "Codigo Digikey"
	1    4500 3100
	1    0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5DDD12ED
P 5200 3100
AR Path="/5DDD12ED" Ref="R?"  Part="1" 
AR Path="/5DCC1C1C/5DDD12ED" Ref="R40"  Part="1" 
F 0 "R40" V 5100 3100 50  0000 C CNN
F 1 "1M" V 5200 3100 50  0000 C CNN
F 2 "lib_fp:R_0603" V 5130 3100 50  0001 C CNN
F 3 "https://www.yageo.com/documents/recent/PYu-RV_51_RoHS_L_8.pdf" H 5200 3100 50  0001 C CNN
F 4 "RES SMD 1M OHM 1% 1/10W 0603" V 5200 3100 50  0001 C CNN "Descripcion"
F 5 "Yageo" V 5200 3100 50  0001 C CNN "Fabricante"
F 6 "RC0603FR-071ML" V 5200 3100 50  0001 C CNN "Codigo Fabricante"
F 7 "311-1.00MHRCT-ND" V 5200 3100 50  0001 C CNN "Codigo Digikey"
	1    5200 3100
	1    0    0    1   
$EndComp
$Comp
L Device:C C4
U 1 1 5DDE4E4C
P 4850 3300
F 0 "C4" V 4598 3300 50  0000 C CNN
F 1 "1uF" V 4689 3300 50  0000 C CNN
F 2 "lib_fp:C_0603" H 4888 3150 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0603C105K4RACTU.pdf" H 4850 3300 50  0001 C CNN
F 4 "CAP CER 1UF 16V X7R 0603" V 4850 3300 50  0001 C CNN "Descripcion"
F 5 "KEMET" V 4850 3300 50  0001 C CNN "Fabricante"
F 6 "C0603C105K4RACTU" V 4850 3300 50  0001 C CNN "Codigo Fabricante"
F 7 "399-7847-1-ND" V 4850 3300 50  0001 C CNN "Codigo Digikey"
	1    4850 3300
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 5DDE4E92
P 7250 3200
F 0 "C3" V 6998 3200 50  0000 C CNN
F 1 "0.1uF" V 7089 3200 50  0000 C CNN
F 2 "lib_fp:C_0603" H 7288 3050 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0603C104Z3VACTU.pdf" H 7250 3200 50  0001 C CNN
F 4 "CAP CER 0.1UF 25V Y5V 0603" V 7250 3200 50  0001 C CNN "Descripcion"
F 5 "KEMET" V 7250 3200 50  0001 C CNN "Fabricante"
F 6 "C0603C104Z3VACTU" V 7250 3200 50  0001 C CNN "Codigo Fabricante"
F 7 "399-1100-1-ND" V 7250 3200 50  0001 C CNN "Codigo Digikey"
	1    7250 3200
	0    1    1    0   
$EndComp
$EndSCHEMATC
