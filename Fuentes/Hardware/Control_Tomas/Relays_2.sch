EESchema Schematic File Version 4
LIBS:Control_Tomas-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Transistor_Array:ULN2803A U?
U 1 1 5DC460EF
P 4750 3750
AR Path="/5DC460EF" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DC460EF" Ref="U?"  Part="1" 
AR Path="/5E07C1CD/5DC460EF" Ref="U6"  Part="1" 
F 0 "U6" H 4750 4317 50  0000 C CNN
F 1 "ULN2803A" H 4750 4226 50  0000 C CNN
F 2 "lib_fp:DIP-18" H 4800 3100 50  0001 L CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/e4/fa/1c/16/4e/39/49/04/CD00000179.pdf/files/CD00000179.pdf/jcr:content/translations/en.CD00000179.pdf" H 4850 3550 50  0001 C CNN
F 4 "TRANS 8NPN DARL 50V 0.5A 18DIP" H 4750 3750 50  0001 C CNN "Descripcion"
F 5 "STMicroelectronics" H 4750 3750 50  0001 C CNN "Fabricante"
F 6 "ULN2803A" H 4750 3750 50  0001 C CNN "Codigo Fabricante"
F 7 "497-2356-5-ND" H 4750 3750 50  0001 C CNN "Codigo Digikey"
	1    4750 3750
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR?
U 1 1 5DCD2254
P 4750 4500
AR Path="/5DCD2254" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD2254" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DCD2254" Ref="#PWR063"  Part="1" 
F 0 "#PWR063" H 4750 4250 50  0001 C CNN
F 1 "Earth" H 4750 4350 50  0001 C CNN
F 2 "" H 4750 4500 50  0001 C CNN
F 3 "~" H 4750 4500 50  0001 C CNN
	1    4750 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 4500 4750 4450
$Comp
L power:+12V #PWR?
U 1 1 5DCD2255
P 5225 3200
AR Path="/5DCD2255" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD2255" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DCD2255" Ref="#PWR054"  Part="1" 
F 0 "#PWR054" H 5225 3050 50  0001 C CNN
F 1 "+12V" H 5240 3373 50  0000 C CNN
F 2 "" H 5225 3200 50  0001 C CNN
F 3 "" H 5225 3200 50  0001 C CNN
	1    5225 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5DCD2257
P 1800 1675
AR Path="/5DCD2257" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD2257" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DCD2257" Ref="#PWR046"  Part="1" 
F 0 "#PWR046" H 1800 1525 50  0001 C CNN
F 1 "+3.3V" H 1815 1848 50  0000 C CNN
F 2 "" H 1800 1675 50  0001 C CNN
F 3 "" H 1800 1675 50  0001 C CNN
	1    1800 1675
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DCD2258
P 3400 1950
AR Path="/5DCD2258" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD2258" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DCD2258" Ref="R22"  Part="1" 
F 0 "R22" V 3193 1950 50  0000 C CNN
F 1 "470" V 3284 1950 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 1950 50  0001 C CNN
F 3 "~" H 3400 1950 50  0001 C CNN
	1    3400 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 1950 3250 1950
$Comp
L power:+12V #PWR?
U 1 1 5DCD2259
P 3100 1675
AR Path="/5DCD2259" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD2259" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DCD2259" Ref="#PWR047"  Part="1" 
F 0 "#PWR047" H 3100 1525 50  0001 C CNN
F 1 "+12V" H 3115 1848 50  0000 C CNN
F 2 "" H 3100 1675 50  0001 C CNN
F 3 "" H 3100 1675 50  0001 C CNN
	1    3100 1675
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 1750 3100 1750
$Comp
L Device:R R?
U 1 1 5DC47537
P 2100 1750
AR Path="/5DC47537" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC47537" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DC47537" Ref="R21"  Part="1" 
F 0 "R21" V 2307 1750 50  0000 C CNN
F 1 "470" V 2216 1750 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 1750 50  0001 C CNN
F 3 "~" H 2100 1750 50  0001 C CNN
	1    2100 1750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 1750 2250 1750
Wire Wire Line
	1800 1675 1800 1750
Text HLabel 1650 1950 0    50   Input ~ 0
IN_1
Wire Wire Line
	1950 1750 1800 1750
Wire Wire Line
	3100 1750 3100 1675
Wire Wire Line
	2400 1950 1650 1950
$Comp
L power:+3.3V #PWR?
U 1 1 5DCD225C
P 1800 2225
AR Path="/5DCD225C" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD225C" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DCD225C" Ref="#PWR049"  Part="1" 
F 0 "#PWR049" H 1800 2075 50  0001 C CNN
F 1 "+3.3V" H 1815 2398 50  0000 C CNN
F 2 "" H 1800 2225 50  0001 C CNN
F 3 "" H 1800 2225 50  0001 C CNN
	1    1800 2225
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DCD225D
P 3400 2500
AR Path="/5DCD225D" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD225D" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DCD225D" Ref="R24"  Part="1" 
F 0 "R24" V 3193 2500 50  0000 C CNN
F 1 "470" V 3284 2500 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 2500 50  0001 C CNN
F 3 "~" H 3400 2500 50  0001 C CNN
	1    3400 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 2500 3250 2500
$Comp
L power:+12V #PWR?
U 1 1 5DCD225E
P 3100 2225
AR Path="/5DCD225E" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD225E" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DCD225E" Ref="#PWR050"  Part="1" 
F 0 "#PWR050" H 3100 2075 50  0001 C CNN
F 1 "+12V" H 3115 2398 50  0000 C CNN
F 2 "" H 3100 2225 50  0001 C CNN
F 3 "" H 3100 2225 50  0001 C CNN
	1    3100 2225
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2300 3100 2300
$Comp
L Device:R R?
U 1 1 5DC4A65B
P 2100 2300
AR Path="/5DC4A65B" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC4A65B" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DC4A65B" Ref="R23"  Part="1" 
F 0 "R23" V 2307 2300 50  0000 C CNN
F 1 "470" V 2216 2300 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 2300 50  0001 C CNN
F 3 "~" H 2100 2300 50  0001 C CNN
	1    2100 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 2300 2250 2300
Wire Wire Line
	1800 2225 1800 2300
Text HLabel 1650 2500 0    50   Input ~ 0
IN_2
Wire Wire Line
	1950 2300 1800 2300
Wire Wire Line
	3100 2300 3100 2225
Wire Wire Line
	2400 2500 1650 2500
$Comp
L power:+3.3V #PWR?
U 1 1 5DC4BED3
P 1800 2800
AR Path="/5DC4BED3" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4BED3" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DC4BED3" Ref="#PWR052"  Part="1" 
F 0 "#PWR052" H 1800 2650 50  0001 C CNN
F 1 "+3.3V" H 1815 2973 50  0000 C CNN
F 2 "" H 1800 2800 50  0001 C CNN
F 3 "" H 1800 2800 50  0001 C CNN
	1    1800 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DCD2262
P 3400 3075
AR Path="/5DCD2262" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD2262" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DCD2262" Ref="R26"  Part="1" 
F 0 "R26" V 3193 3075 50  0000 C CNN
F 1 "470" V 3284 3075 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 3075 50  0001 C CNN
F 3 "~" H 3400 3075 50  0001 C CNN
	1    3400 3075
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 3075 3250 3075
$Comp
L power:+12V #PWR?
U 1 1 5DCD2263
P 3100 2800
AR Path="/5DCD2263" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD2263" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DCD2263" Ref="#PWR053"  Part="1" 
F 0 "#PWR053" H 3100 2650 50  0001 C CNN
F 1 "+12V" H 3115 2973 50  0000 C CNN
F 2 "" H 3100 2800 50  0001 C CNN
F 3 "" H 3100 2800 50  0001 C CNN
	1    3100 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2875 3100 2875
$Comp
L Device:R R?
U 1 1 5DCD2264
P 2100 2875
AR Path="/5DCD2264" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD2264" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DCD2264" Ref="R25"  Part="1" 
F 0 "R25" V 2307 2875 50  0000 C CNN
F 1 "470" V 2216 2875 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 2875 50  0001 C CNN
F 3 "~" H 2100 2875 50  0001 C CNN
	1    2100 2875
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 2875 2250 2875
Wire Wire Line
	1800 2800 1800 2875
Text HLabel 1650 3075 0    50   Input ~ 0
IN_3
Wire Wire Line
	1950 2875 1800 2875
Wire Wire Line
	3100 2875 3100 2800
Wire Wire Line
	2400 3075 1650 3075
$Comp
L power:+3.3V #PWR?
U 1 1 5DCD2266
P 1800 3350
AR Path="/5DCD2266" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD2266" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DCD2266" Ref="#PWR055"  Part="1" 
F 0 "#PWR055" H 1800 3200 50  0001 C CNN
F 1 "+3.3V" H 1815 3523 50  0000 C CNN
F 2 "" H 1800 3350 50  0001 C CNN
F 3 "" H 1800 3350 50  0001 C CNN
	1    1800 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DC4BF00
P 3400 3625
AR Path="/5DC4BF00" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC4BF00" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DC4BF00" Ref="R28"  Part="1" 
F 0 "R28" V 3193 3625 50  0000 C CNN
F 1 "470" V 3284 3625 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 3625 50  0001 C CNN
F 3 "~" H 3400 3625 50  0001 C CNN
	1    3400 3625
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 3625 3250 3625
$Comp
L power:+12V #PWR?
U 1 1 5DCD2268
P 3100 3350
AR Path="/5DCD2268" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD2268" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DCD2268" Ref="#PWR056"  Part="1" 
F 0 "#PWR056" H 3100 3200 50  0001 C CNN
F 1 "+12V" H 3115 3523 50  0000 C CNN
F 2 "" H 3100 3350 50  0001 C CNN
F 3 "" H 3100 3350 50  0001 C CNN
	1    3100 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 3425 3100 3425
$Comp
L Device:R R?
U 1 1 5DCD2269
P 2100 3425
AR Path="/5DCD2269" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD2269" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DCD2269" Ref="R27"  Part="1" 
F 0 "R27" V 2307 3425 50  0000 C CNN
F 1 "470" V 2216 3425 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 3425 50  0001 C CNN
F 3 "~" H 2100 3425 50  0001 C CNN
	1    2100 3425
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 3425 2250 3425
Wire Wire Line
	1800 3350 1800 3425
Text HLabel 1650 3625 0    50   Input ~ 0
IN_4
Wire Wire Line
	1950 3425 1800 3425
Wire Wire Line
	3100 3425 3100 3350
Wire Wire Line
	2400 3625 1650 3625
$Comp
L power:+3.3V #PWR?
U 1 1 5DCD226B
P 1800 3925
AR Path="/5DCD226B" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD226B" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DCD226B" Ref="#PWR059"  Part="1" 
F 0 "#PWR059" H 1800 3775 50  0001 C CNN
F 1 "+3.3V" H 1815 4098 50  0000 C CNN
F 2 "" H 1800 3925 50  0001 C CNN
F 3 "" H 1800 3925 50  0001 C CNN
	1    1800 3925
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DCD226C
P 3400 4200
AR Path="/5DCD226C" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD226C" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DCD226C" Ref="R30"  Part="1" 
F 0 "R30" V 3193 4200 50  0000 C CNN
F 1 "470" V 3284 4200 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 4200 50  0001 C CNN
F 3 "~" H 3400 4200 50  0001 C CNN
	1    3400 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 4200 3250 4200
$Comp
L power:+12V #PWR?
U 1 1 5DC4CFA2
P 3100 3925
AR Path="/5DC4CFA2" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4CFA2" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DC4CFA2" Ref="#PWR060"  Part="1" 
F 0 "#PWR060" H 3100 3775 50  0001 C CNN
F 1 "+12V" H 3115 4098 50  0000 C CNN
F 2 "" H 3100 3925 50  0001 C CNN
F 3 "" H 3100 3925 50  0001 C CNN
	1    3100 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 4000 3100 4000
$Comp
L Device:R R?
U 1 1 5DC4CFA9
P 2100 4000
AR Path="/5DC4CFA9" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC4CFA9" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DC4CFA9" Ref="R29"  Part="1" 
F 0 "R29" V 2307 4000 50  0000 C CNN
F 1 "470" V 2216 4000 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 4000 50  0001 C CNN
F 3 "~" H 2100 4000 50  0001 C CNN
	1    2100 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 4000 2250 4000
Wire Wire Line
	1800 3925 1800 4000
Text HLabel 1650 4200 0    50   Input ~ 0
IN_5
Wire Wire Line
	1950 4000 1800 4000
Wire Wire Line
	3100 4000 3100 3925
Wire Wire Line
	2400 4200 1650 4200
$Comp
L power:+3.3V #PWR?
U 1 1 5DC4CFBB
P 1800 4475
AR Path="/5DC4CFBB" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4CFBB" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DC4CFBB" Ref="#PWR061"  Part="1" 
F 0 "#PWR061" H 1800 4325 50  0001 C CNN
F 1 "+3.3V" H 1815 4648 50  0000 C CNN
F 2 "" H 1800 4475 50  0001 C CNN
F 3 "" H 1800 4475 50  0001 C CNN
	1    1800 4475
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DCD2271
P 3400 4750
AR Path="/5DCD2271" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD2271" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DCD2271" Ref="R32"  Part="1" 
F 0 "R32" V 3193 4750 50  0000 C CNN
F 1 "470" V 3284 4750 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 4750 50  0001 C CNN
F 3 "~" H 3400 4750 50  0001 C CNN
	1    3400 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 4750 3250 4750
$Comp
L power:+12V #PWR?
U 1 1 5DCD2272
P 3100 4475
AR Path="/5DCD2272" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD2272" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DCD2272" Ref="#PWR062"  Part="1" 
F 0 "#PWR062" H 3100 4325 50  0001 C CNN
F 1 "+12V" H 3115 4648 50  0000 C CNN
F 2 "" H 3100 4475 50  0001 C CNN
F 3 "" H 3100 4475 50  0001 C CNN
	1    3100 4475
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 4550 3100 4550
$Comp
L Device:R R?
U 1 1 5DC4CFD0
P 2100 4550
AR Path="/5DC4CFD0" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC4CFD0" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DC4CFD0" Ref="R31"  Part="1" 
F 0 "R31" V 2307 4550 50  0000 C CNN
F 1 "470" V 2216 4550 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 4550 50  0001 C CNN
F 3 "~" H 2100 4550 50  0001 C CNN
	1    2100 4550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 4550 2250 4550
Wire Wire Line
	1800 4475 1800 4550
Text HLabel 1650 4750 0    50   Input ~ 0
IN_6
Wire Wire Line
	1950 4550 1800 4550
Wire Wire Line
	3100 4550 3100 4475
Wire Wire Line
	2400 4750 1650 4750
$Comp
L power:+3.3V #PWR?
U 1 1 5DC4CFE2
P 1800 5050
AR Path="/5DC4CFE2" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4CFE2" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DC4CFE2" Ref="#PWR066"  Part="1" 
F 0 "#PWR066" H 1800 4900 50  0001 C CNN
F 1 "+3.3V" H 1815 5223 50  0000 C CNN
F 2 "" H 1800 5050 50  0001 C CNN
F 3 "" H 1800 5050 50  0001 C CNN
	1    1800 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DC4CFE8
P 3400 5325
AR Path="/5DC4CFE8" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC4CFE8" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DC4CFE8" Ref="R34"  Part="1" 
F 0 "R34" V 3193 5325 50  0000 C CNN
F 1 "470" V 3284 5325 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 5325 50  0001 C CNN
F 3 "~" H 3400 5325 50  0001 C CNN
	1    3400 5325
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 5325 3250 5325
$Comp
L power:+12V #PWR?
U 1 1 5DC4CFF0
P 3100 5050
AR Path="/5DC4CFF0" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4CFF0" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DC4CFF0" Ref="#PWR067"  Part="1" 
F 0 "#PWR067" H 3100 4900 50  0001 C CNN
F 1 "+12V" H 3115 5223 50  0000 C CNN
F 2 "" H 3100 5050 50  0001 C CNN
F 3 "" H 3100 5050 50  0001 C CNN
	1    3100 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 5125 3100 5125
$Comp
L Device:R R?
U 1 1 5DC4CFF7
P 2100 5125
AR Path="/5DC4CFF7" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC4CFF7" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DC4CFF7" Ref="R33"  Part="1" 
F 0 "R33" V 2307 5125 50  0000 C CNN
F 1 "470" V 2216 5125 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 5125 50  0001 C CNN
F 3 "~" H 2100 5125 50  0001 C CNN
	1    2100 5125
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 5125 2250 5125
Wire Wire Line
	1800 5050 1800 5125
Text HLabel 1650 5325 0    50   Input ~ 0
IN_7
Wire Wire Line
	1950 5125 1800 5125
Wire Wire Line
	3100 5125 3100 5050
Wire Wire Line
	2400 5325 1650 5325
$Comp
L power:+3.3V #PWR?
U 1 1 5DC4D009
P 1800 5600
AR Path="/5DC4D009" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4D009" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DC4D009" Ref="#PWR068"  Part="1" 
F 0 "#PWR068" H 1800 5450 50  0001 C CNN
F 1 "+3.3V" H 1815 5773 50  0000 C CNN
F 2 "" H 1800 5600 50  0001 C CNN
F 3 "" H 1800 5600 50  0001 C CNN
	1    1800 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DC4D00F
P 3400 5875
AR Path="/5DC4D00F" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC4D00F" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DC4D00F" Ref="R36"  Part="1" 
F 0 "R36" V 3193 5875 50  0000 C CNN
F 1 "470" V 3284 5875 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 5875 50  0001 C CNN
F 3 "~" H 3400 5875 50  0001 C CNN
	1    3400 5875
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 5875 3250 5875
$Comp
L power:+12V #PWR?
U 1 1 5DCD227C
P 3100 5600
AR Path="/5DCD227C" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD227C" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DCD227C" Ref="#PWR069"  Part="1" 
F 0 "#PWR069" H 3100 5450 50  0001 C CNN
F 1 "+12V" H 3115 5773 50  0000 C CNN
F 2 "" H 3100 5600 50  0001 C CNN
F 3 "" H 3100 5600 50  0001 C CNN
	1    3100 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 5675 3100 5675
$Comp
L Device:R R?
U 1 1 5DCD227D
P 2100 5675
AR Path="/5DCD227D" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD227D" Ref="R?"  Part="1" 
AR Path="/5E07C1CD/5DCD227D" Ref="R35"  Part="1" 
F 0 "R35" V 2307 5675 50  0000 C CNN
F 1 "470" V 2216 5675 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 5675 50  0001 C CNN
F 3 "~" H 2100 5675 50  0001 C CNN
	1    2100 5675
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 5675 2250 5675
Wire Wire Line
	1800 5600 1800 5675
Text HLabel 1650 5875 0    50   Input ~ 0
IN_8
Wire Wire Line
	1950 5675 1800 5675
Wire Wire Line
	3100 5675 3100 5600
Wire Wire Line
	2400 5875 1650 5875
Wire Wire Line
	4350 3550 4250 3550
Wire Wire Line
	4250 3550 4250 1950
Wire Wire Line
	4350 3650 4150 3650
Wire Wire Line
	4150 3650 4150 2500
Wire Wire Line
	4050 3750 4050 3075
Wire Wire Line
	4050 3750 4350 3750
Wire Wire Line
	4350 3850 3950 3850
Wire Wire Line
	3950 3850 3950 3625
Wire Wire Line
	4350 3950 3950 3950
Wire Wire Line
	3950 3950 3950 4200
Wire Wire Line
	4350 4050 4050 4050
Wire Wire Line
	4050 4050 4050 4750
Wire Wire Line
	4350 4150 4150 4150
Wire Wire Line
	4150 4150 4150 5325
Wire Wire Line
	4350 4250 4250 4250
Wire Wire Line
	4250 4250 4250 5875
Wire Wire Line
	5225 3450 5225 3200
Wire Wire Line
	5150 3450 5225 3450
Text Label 5250 3550 0    50   ~ 0
R9
Text Label 5250 3650 0    50   ~ 0
R10
Text Label 5250 3750 0    50   ~ 0
R11
Text Label 5250 3850 0    50   ~ 0
R12
Text Label 5250 3950 0    50   ~ 0
R13
Text Label 5250 4050 0    50   ~ 0
R14
Text Label 5250 4150 0    50   ~ 0
R15
Text Label 5250 4250 0    50   ~ 0
R16
$Comp
L power:+12V #PWR?
U 1 1 5DEBE998
P 6350 825
AR Path="/5DEBE998" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DEBE998" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DEBE998" Ref="#PWR044"  Part="1" 
F 0 "#PWR044" H 6350 675 50  0001 C CNN
F 1 "+12V" H 6365 998 50  0000 C CNN
F 2 "" H 6350 825 50  0001 C CNN
F 3 "" H 6350 825 50  0001 C CNN
	1    6350 825 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 850  6350 825 
Wire Wire Line
	6350 1875 6350 1850
Wire Wire Line
	5150 3550 5400 3550
Wire Wire Line
	5150 3650 5475 3650
Wire Wire Line
	6650 850  6650 825 
Text Label 7125 1850 0    50   ~ 0
Toma9
Text GLabel 6725 825  2    50   BiDi ~ 0
VPOW
Wire Wire Line
	7100 1850 7425 1850
Wire Wire Line
	6800 1850 6650 1850
Wire Wire Line
	6725 825  6650 825 
$Comp
L power:+12V #PWR?
U 1 1 5DEBE9C4
P 6350 2200
AR Path="/5DEBE9C4" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DEBE9C4" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DEBE9C4" Ref="#PWR048"  Part="1" 
F 0 "#PWR048" H 6350 2050 50  0001 C CNN
F 1 "+12V" H 6365 2373 50  0000 C CNN
F 2 "" H 6350 2200 50  0001 C CNN
F 3 "" H 6350 2200 50  0001 C CNN
	1    6350 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2225 6350 2200
Wire Wire Line
	6350 3250 6350 3225
Wire Wire Line
	6650 2225 6650 2200
Text Label 7125 3225 0    50   ~ 0
Toma11
Text GLabel 6725 2200 2    50   BiDi ~ 0
VPOW
Wire Wire Line
	7100 3225 7425 3225
Wire Wire Line
	6800 3225 6650 3225
Wire Wire Line
	6725 2200 6650 2200
$Comp
L power:+12V #PWR?
U 1 1 5DEBE9EE
P 6350 3575
AR Path="/5DEBE9EE" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DEBE9EE" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DEBE9EE" Ref="#PWR057"  Part="1" 
F 0 "#PWR057" H 6350 3425 50  0001 C CNN
F 1 "+12V" H 6365 3748 50  0000 C CNN
F 2 "" H 6350 3575 50  0001 C CNN
F 3 "" H 6350 3575 50  0001 C CNN
	1    6350 3575
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3600 6350 3575
Wire Wire Line
	6350 4625 6350 4600
Wire Wire Line
	6650 3600 6650 3575
Text Label 7125 4600 0    50   ~ 0
Toma13
Text GLabel 6725 3575 2    50   BiDi ~ 0
VPOW
Wire Wire Line
	7100 4600 7425 4600
Wire Wire Line
	6800 4600 6650 4600
Wire Wire Line
	6725 3575 6650 3575
$Comp
L power:+12V #PWR?
U 1 1 5DEBEA18
P 6350 4950
AR Path="/5DEBEA18" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DEBEA18" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DEBEA18" Ref="#PWR064"  Part="1" 
F 0 "#PWR064" H 6350 4800 50  0001 C CNN
F 1 "+12V" H 6365 5123 50  0000 C CNN
F 2 "" H 6350 4950 50  0001 C CNN
F 3 "" H 6350 4950 50  0001 C CNN
	1    6350 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 4975 6350 4950
Wire Wire Line
	6350 6000 6350 5975
Wire Wire Line
	6650 4975 6650 4950
Text Label 7125 5975 0    50   ~ 0
Toma15
Text GLabel 6725 4950 2    50   BiDi ~ 0
VPOW
Wire Wire Line
	7100 5975 7425 5975
Wire Wire Line
	6800 5975 6650 5975
Wire Wire Line
	6725 4950 6650 4950
Wire Wire Line
	5400 1875 5400 3550
Wire Wire Line
	5400 1875 6350 1875
$Comp
L power:+12V #PWR?
U 1 1 5DEBEA44
P 8925 850
AR Path="/5DEBEA44" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DEBEA44" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DEBEA44" Ref="#PWR045"  Part="1" 
F 0 "#PWR045" H 8925 700 50  0001 C CNN
F 1 "+12V" H 8940 1023 50  0000 C CNN
F 2 "" H 8925 850 50  0001 C CNN
F 3 "" H 8925 850 50  0001 C CNN
	1    8925 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8925 875  8925 850 
Wire Wire Line
	9225 875  9225 850 
Text Label 9700 1875 0    50   ~ 0
Toma10
Text GLabel 9300 850  2    50   BiDi ~ 0
VPOW
Wire Wire Line
	9675 1875 10000 1875
Wire Wire Line
	9375 1875 9225 1875
Wire Wire Line
	9300 850  9225 850 
$Comp
L power:+12V #PWR?
U 1 1 5DEBEA6D
P 8925 2225
AR Path="/5DEBEA6D" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DEBEA6D" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DEBEA6D" Ref="#PWR051"  Part="1" 
F 0 "#PWR051" H 8925 2075 50  0001 C CNN
F 1 "+12V" H 8940 2398 50  0000 C CNN
F 2 "" H 8925 2225 50  0001 C CNN
F 3 "" H 8925 2225 50  0001 C CNN
	1    8925 2225
	1    0    0    -1  
$EndComp
Wire Wire Line
	8925 2250 8925 2225
Wire Wire Line
	9225 2250 9225 2225
Text Label 9700 3250 0    50   ~ 0
Toma12
Text GLabel 9300 2225 2    50   BiDi ~ 0
VPOW
Wire Wire Line
	9675 3250 10000 3250
Wire Wire Line
	9375 3250 9225 3250
Wire Wire Line
	9300 2225 9225 2225
$Comp
L power:+12V #PWR?
U 1 1 5DEBEA96
P 8925 3600
AR Path="/5DEBEA96" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DEBEA96" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DEBEA96" Ref="#PWR058"  Part="1" 
F 0 "#PWR058" H 8925 3450 50  0001 C CNN
F 1 "+12V" H 8940 3773 50  0000 C CNN
F 2 "" H 8925 3600 50  0001 C CNN
F 3 "" H 8925 3600 50  0001 C CNN
	1    8925 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8925 3625 8925 3600
Wire Wire Line
	9225 3625 9225 3600
Text Label 9700 4625 0    50   ~ 0
Toma14
Text GLabel 9300 3600 2    50   BiDi ~ 0
VPOW
Wire Wire Line
	9675 4625 10000 4625
Wire Wire Line
	9375 4625 9225 4625
Wire Wire Line
	9300 3600 9225 3600
$Comp
L power:+12V #PWR?
U 1 1 5DEBEABF
P 8925 4975
AR Path="/5DEBEABF" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DEBEABF" Ref="#PWR?"  Part="1" 
AR Path="/5E07C1CD/5DEBEABF" Ref="#PWR065"  Part="1" 
F 0 "#PWR065" H 8925 4825 50  0001 C CNN
F 1 "+12V" H 8940 5148 50  0000 C CNN
F 2 "" H 8925 4975 50  0001 C CNN
F 3 "" H 8925 4975 50  0001 C CNN
	1    8925 4975
	1    0    0    -1  
$EndComp
Wire Wire Line
	8925 5000 8925 4975
Wire Wire Line
	9225 5000 9225 4975
Text Label 9700 6000 0    50   ~ 0
Toma16
Text GLabel 9300 4975 2    50   BiDi ~ 0
VPOW
Wire Wire Line
	9675 6000 10000 6000
Wire Wire Line
	9375 6000 9225 6000
Wire Wire Line
	9300 4975 9225 4975
Wire Wire Line
	5475 1950 8925 1950
Wire Wire Line
	8925 1875 8925 1950
Wire Wire Line
	5150 3750 5550 3750
Wire Wire Line
	5550 3750 5550 3250
Wire Wire Line
	5550 3250 6350 3250
Wire Wire Line
	5150 3850 5625 3850
Wire Wire Line
	5625 3850 5625 3325
Wire Wire Line
	5625 3325 8925 3325
Wire Wire Line
	8925 3250 8925 3325
Wire Wire Line
	5150 3950 5625 3950
Wire Wire Line
	5625 3950 5625 4625
Wire Wire Line
	5625 4625 6350 4625
Wire Wire Line
	5150 4050 5550 4050
Wire Wire Line
	5550 4050 5550 4700
Wire Wire Line
	5550 4700 8925 4700
Wire Wire Line
	8925 4625 8925 4700
Wire Wire Line
	5150 4150 5475 4150
Wire Wire Line
	5475 4150 5475 6000
Wire Wire Line
	5475 6000 6350 6000
Wire Wire Line
	5150 4250 5400 4250
Wire Wire Line
	5400 4250 5400 6075
Wire Wire Line
	5400 6075 8925 6075
Wire Wire Line
	8925 6000 8925 6075
Wire Wire Line
	5475 1950 5475 3650
$Comp
L Device:Polyfuse F?
U 1 1 5DE1B228
P 6950 1850
AR Path="/5DC45DB1/5DE1B228" Ref="F?"  Part="1" 
AR Path="/5E07C1CD/5DE1B228" Ref="F9"  Part="1" 
F 0 "F9" V 6725 1850 50  0000 C CNN
F 1 "Polyfuse" V 6816 1850 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 7000 1650 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 6950 1850 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 6950 1850 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 6950 1850 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 6950 1850 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 6950 1850 50  0001 C CNN "Codigo Digikey"
	1    6950 1850
	0    1    1    0   
$EndComp
$Comp
L Device:Polyfuse F?
U 1 1 5DE209FC
P 9525 1875
AR Path="/5DC45DB1/5DE209FC" Ref="F?"  Part="1" 
AR Path="/5E07C1CD/5DE209FC" Ref="F10"  Part="1" 
F 0 "F10" V 9300 1875 50  0000 C CNN
F 1 "Polyfuse" V 9391 1875 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 9575 1675 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 9525 1875 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 9525 1875 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 9525 1875 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 9525 1875 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 9525 1875 50  0001 C CNN "Codigo Digikey"
	1    9525 1875
	0    1    1    0   
$EndComp
$Comp
L Device:Polyfuse F?
U 1 1 5DE2631D
P 6950 3225
AR Path="/5DC45DB1/5DE2631D" Ref="F?"  Part="1" 
AR Path="/5E07C1CD/5DE2631D" Ref="F11"  Part="1" 
F 0 "F11" V 6725 3225 50  0000 C CNN
F 1 "Polyfuse" V 6816 3225 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 7000 3025 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 6950 3225 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 6950 3225 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 6950 3225 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 6950 3225 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 6950 3225 50  0001 C CNN "Codigo Digikey"
	1    6950 3225
	0    1    1    0   
$EndComp
$Comp
L Device:Polyfuse F?
U 1 1 5DE2BCFB
P 9525 3250
AR Path="/5DC45DB1/5DE2BCFB" Ref="F?"  Part="1" 
AR Path="/5E07C1CD/5DE2BCFB" Ref="F12"  Part="1" 
F 0 "F12" V 9300 3250 50  0000 C CNN
F 1 "Polyfuse" V 9391 3250 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 9575 3050 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 9525 3250 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 9525 3250 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 9525 3250 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 9525 3250 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 9525 3250 50  0001 C CNN "Codigo Digikey"
	1    9525 3250
	0    1    1    0   
$EndComp
$Comp
L Device:Polyfuse F?
U 1 1 5DE31BFD
P 6950 4600
AR Path="/5DC45DB1/5DE31BFD" Ref="F?"  Part="1" 
AR Path="/5E07C1CD/5DE31BFD" Ref="F13"  Part="1" 
F 0 "F13" V 6725 4600 50  0000 C CNN
F 1 "Polyfuse" V 6816 4600 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 7000 4400 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 6950 4600 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 6950 4600 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 6950 4600 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 6950 4600 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 6950 4600 50  0001 C CNN "Codigo Digikey"
	1    6950 4600
	0    1    1    0   
$EndComp
$Comp
L Device:Polyfuse F?
U 1 1 5DE373EF
P 6950 5975
AR Path="/5DC45DB1/5DE373EF" Ref="F?"  Part="1" 
AR Path="/5E07C1CD/5DE373EF" Ref="F15"  Part="1" 
F 0 "F15" V 6725 5975 50  0000 C CNN
F 1 "Polyfuse" V 6816 5975 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 7000 5775 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 6950 5975 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 6950 5975 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 6950 5975 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 6950 5975 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 6950 5975 50  0001 C CNN "Codigo Digikey"
	1    6950 5975
	0    1    1    0   
$EndComp
$Comp
L Device:Polyfuse F?
U 1 1 5DE3CBD8
P 9525 4625
AR Path="/5DC45DB1/5DE3CBD8" Ref="F?"  Part="1" 
AR Path="/5E07C1CD/5DE3CBD8" Ref="F14"  Part="1" 
F 0 "F14" V 9300 4625 50  0000 C CNN
F 1 "Polyfuse" V 9391 4625 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 9575 4425 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 9525 4625 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 9525 4625 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 9525 4625 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 9525 4625 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 9525 4625 50  0001 C CNN "Codigo Digikey"
	1    9525 4625
	0    1    1    0   
$EndComp
$Comp
L Device:Polyfuse F?
U 1 1 5DE423AC
P 9525 6000
AR Path="/5DC45DB1/5DE423AC" Ref="F?"  Part="1" 
AR Path="/5E07C1CD/5DE423AC" Ref="F16"  Part="1" 
F 0 "F16" V 9300 6000 50  0000 C CNN
F 1 "Polyfuse" V 9391 6000 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 9575 5800 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 9525 6000 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 9525 6000 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 9525 6000 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 9525 6000 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 9525 6000 50  0001 C CNN "Codigo Digikey"
	1    9525 6000
	0    1    1    0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J?
U 1 1 5DE72E26
P 7625 1850
AR Path="/5DC45DB1/5DE72E26" Ref="J?"  Part="1" 
AR Path="/5E07C1CD/5DE72E26" Ref="J12"  Part="1" 
F 0 "J12" H 7705 1892 50  0000 L CNN
F 1 "Toma09" H 7705 1801 50  0000 L CNN
F 2 "lib_fp:1704994" H 7625 1850 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 7625 1850 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 7625 1850 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 7625 1850 50  0001 C CNN "Fabricante"
F 6 "1704994" H 7625 1850 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 7625 1850 50  0001 C CNN "Codigo Digikey"
	1    7625 1850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J?
U 1 1 5DE79E98
P 10200 1875
AR Path="/5DC45DB1/5DE79E98" Ref="J?"  Part="1" 
AR Path="/5E07C1CD/5DE79E98" Ref="J13"  Part="1" 
F 0 "J13" H 10280 1917 50  0000 L CNN
F 1 "Toma10" H 10280 1826 50  0000 L CNN
F 2 "lib_fp:1704994" H 10200 1875 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 10200 1875 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 10200 1875 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 10200 1875 50  0001 C CNN "Fabricante"
F 6 "1704994" H 10200 1875 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 10200 1875 50  0001 C CNN "Codigo Digikey"
	1    10200 1875
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J?
U 1 1 5DE7F67F
P 7625 3225
AR Path="/5DC45DB1/5DE7F67F" Ref="J?"  Part="1" 
AR Path="/5E07C1CD/5DE7F67F" Ref="J14"  Part="1" 
F 0 "J14" H 7705 3267 50  0000 L CNN
F 1 "Toma11" H 7705 3176 50  0000 L CNN
F 2 "lib_fp:1704994" H 7625 3225 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 7625 3225 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 7625 3225 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 7625 3225 50  0001 C CNN "Fabricante"
F 6 "1704994" H 7625 3225 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 7625 3225 50  0001 C CNN "Codigo Digikey"
	1    7625 3225
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J?
U 1 1 5DE84E55
P 10200 3250
AR Path="/5DC45DB1/5DE84E55" Ref="J?"  Part="1" 
AR Path="/5E07C1CD/5DE84E55" Ref="J15"  Part="1" 
F 0 "J15" H 10280 3292 50  0000 L CNN
F 1 "Toma12" H 10280 3201 50  0000 L CNN
F 2 "lib_fp:1704994" H 10200 3250 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 10200 3250 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 10200 3250 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 10200 3250 50  0001 C CNN "Fabricante"
F 6 "1704994" H 10200 3250 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 10200 3250 50  0001 C CNN "Codigo Digikey"
	1    10200 3250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J?
U 1 1 5DE8AAC6
P 7625 4600
AR Path="/5DC45DB1/5DE8AAC6" Ref="J?"  Part="1" 
AR Path="/5E07C1CD/5DE8AAC6" Ref="J16"  Part="1" 
F 0 "J16" H 7705 4642 50  0000 L CNN
F 1 "Toma13" H 7705 4551 50  0000 L CNN
F 2 "lib_fp:1704994" H 7625 4600 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 7625 4600 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 7625 4600 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 7625 4600 50  0001 C CNN "Fabricante"
F 6 "1704994" H 7625 4600 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 7625 4600 50  0001 C CNN "Codigo Digikey"
	1    7625 4600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J?
U 1 1 5DE902CA
P 10200 4625
AR Path="/5DC45DB1/5DE902CA" Ref="J?"  Part="1" 
AR Path="/5E07C1CD/5DE902CA" Ref="J17"  Part="1" 
F 0 "J17" H 10280 4667 50  0000 L CNN
F 1 "Toma14" H 10280 4576 50  0000 L CNN
F 2 "lib_fp:1704994" H 10200 4625 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 10200 4625 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 10200 4625 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 10200 4625 50  0001 C CNN "Fabricante"
F 6 "1704994" H 10200 4625 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 10200 4625 50  0001 C CNN "Codigo Digikey"
	1    10200 4625
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J?
U 1 1 5DE95DD4
P 7625 5975
AR Path="/5DC45DB1/5DE95DD4" Ref="J?"  Part="1" 
AR Path="/5E07C1CD/5DE95DD4" Ref="J18"  Part="1" 
F 0 "J18" H 7705 6017 50  0000 L CNN
F 1 "Toma15" H 7705 5926 50  0000 L CNN
F 2 "lib_fp:1704994" H 7625 5975 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 7625 5975 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 7625 5975 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 7625 5975 50  0001 C CNN "Fabricante"
F 6 "1704994" H 7625 5975 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 7625 5975 50  0001 C CNN "Codigo Digikey"
	1    7625 5975
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J?
U 1 1 5DE9B5C6
P 10200 6000
AR Path="/5DC45DB1/5DE9B5C6" Ref="J?"  Part="1" 
AR Path="/5E07C1CD/5DE9B5C6" Ref="J19"  Part="1" 
F 0 "J19" H 10280 6042 50  0000 L CNN
F 1 "Toma16" H 10280 5951 50  0000 L CNN
F 2 "lib_fp:1704994" H 10200 6000 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 10200 6000 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 10200 6000 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 10200 6000 50  0001 C CNN "Fabricante"
F 6 "1704994" H 10200 6000 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 10200 6000 50  0001 C CNN "Codigo Digikey"
	1    10200 6000
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 1 1 5DDEBC70
P 2700 1850
AR Path="/5DDEBC70" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDEBC70" Ref="U?"  Part="1" 
AR Path="/5E07C1CD/5DDEBC70" Ref="U5"  Part="1" 
F 0 "U5" H 2700 2175 50  0000 C CNN
F 1 "TLP291-4" H 2700 2084 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 1650 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 1850 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 1850 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 1850 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 1850 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 1850 50  0001 C CNN "Codigo Digikey"
	1    2700 1850
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 2 1 5DDEBC7B
P 2700 2400
AR Path="/5DDEBC7B" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDEBC7B" Ref="U?"  Part="2" 
AR Path="/5E07C1CD/5DDEBC7B" Ref="U5"  Part="2" 
F 0 "U5" H 2700 2725 50  0000 C CNN
F 1 "TLP291-4" H 2700 2634 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 2200 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 2400 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 2400 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 2400 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 2400 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 2400 50  0001 C CNN "Codigo Digikey"
	2    2700 2400
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 3 1 5DDEBC86
P 2700 2975
AR Path="/5DDEBC86" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDEBC86" Ref="U?"  Part="3" 
AR Path="/5E07C1CD/5DDEBC86" Ref="U5"  Part="3" 
F 0 "U5" H 2700 3300 50  0000 C CNN
F 1 "TLP291-4" H 2700 3209 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 2775 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 2975 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 2975 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 2975 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 2975 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 2975 50  0001 C CNN "Codigo Digikey"
	3    2700 2975
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 4 1 5DDEBC91
P 2700 3525
AR Path="/5DDEBC91" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDEBC91" Ref="U?"  Part="4" 
AR Path="/5E07C1CD/5DDEBC91" Ref="U5"  Part="4" 
F 0 "U5" H 2700 3850 50  0000 C CNN
F 1 "TLP291-4" H 2700 3759 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 3325 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 3525 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 3525 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 3525 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 3525 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 3525 50  0001 C CNN "Codigo Digikey"
	4    2700 3525
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 1 1 5DDF18B3
P 2700 4100
AR Path="/5DDF18B3" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDF18B3" Ref="U?"  Part="1" 
AR Path="/5E07C1CD/5DDF18B3" Ref="U7"  Part="1" 
F 0 "U7" H 2700 4425 50  0000 C CNN
F 1 "TLP291-4" H 2700 4334 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 3900 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 4100 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 4100 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 4100 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 4100 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 4100 50  0001 C CNN "Codigo Digikey"
	1    2700 4100
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 2 1 5DDF18BE
P 2700 4650
AR Path="/5DDF18BE" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDF18BE" Ref="U?"  Part="2" 
AR Path="/5E07C1CD/5DDF18BE" Ref="U7"  Part="2" 
F 0 "U7" H 2700 4975 50  0000 C CNN
F 1 "TLP291-4" H 2700 4884 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 4450 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 4650 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 4650 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 4650 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 4650 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 4650 50  0001 C CNN "Codigo Digikey"
	2    2700 4650
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 3 1 5DDF18C9
P 2700 5225
AR Path="/5DDF18C9" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDF18C9" Ref="U?"  Part="3" 
AR Path="/5E07C1CD/5DDF18C9" Ref="U7"  Part="3" 
F 0 "U7" H 2700 5550 50  0000 C CNN
F 1 "TLP291-4" H 2700 5459 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 5025 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 5225 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 5225 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 5225 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 5225 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 5225 50  0001 C CNN "Codigo Digikey"
	3    2700 5225
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 4 1 5DDF18D4
P 2700 5775
AR Path="/5DDF18D4" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDF18D4" Ref="U?"  Part="4" 
AR Path="/5E07C1CD/5DDF18D4" Ref="U7"  Part="4" 
F 0 "U7" H 2700 6100 50  0000 C CNN
F 1 "TLP291-4" H 2700 6009 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 5575 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 5775 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 5775 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 5775 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 5775 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 5775 50  0001 C CNN "Codigo Digikey"
	4    2700 5775
	1    0    0    -1  
$EndComp
$Comp
L 1461402-6:1461402-6 K?
U 1 1 5DE190BC
P 6550 1350
AR Path="/5DC45DB1/5DE190BC" Ref="K?"  Part="1" 
AR Path="/5E07C1CD/5DE190BC" Ref="K9"  Part="1" 
F 0 "K9" V 6596 1120 50  0000 R CNN
F 1 "1461402-6" V 6505 1120 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 6550 1350 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 6550 1350 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 6550 1350 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 6550 1350 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 6550 1350 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 6550 1350 50  0001 L BNN "Codigo Digikey"
	1    6550 1350
	0    -1   -1   0   
$EndComp
$Comp
L 1461402-6:1461402-6 K?
U 1 1 5DE1E87A
P 9125 1375
AR Path="/5DC45DB1/5DE1E87A" Ref="K?"  Part="1" 
AR Path="/5E07C1CD/5DE1E87A" Ref="K10"  Part="1" 
F 0 "K10" V 9171 1145 50  0000 R CNN
F 1 "1461402-6" V 9080 1145 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 9125 1375 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 9125 1375 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 9125 1375 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 9125 1375 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 9125 1375 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 9125 1375 50  0001 L BNN "Codigo Digikey"
	1    9125 1375
	0    -1   -1   0   
$EndComp
$Comp
L 1461402-6:1461402-6 K?
U 1 1 5DE24284
P 6550 2725
AR Path="/5DC45DB1/5DE24284" Ref="K?"  Part="1" 
AR Path="/5E07C1CD/5DE24284" Ref="K11"  Part="1" 
F 0 "K11" V 6596 2495 50  0000 R CNN
F 1 "1461402-6" V 6505 2495 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 6550 2725 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 6550 2725 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 6550 2725 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 6550 2725 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 6550 2725 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 6550 2725 50  0001 L BNN "Codigo Digikey"
	1    6550 2725
	0    -1   -1   0   
$EndComp
$Comp
L 1461402-6:1461402-6 K?
U 1 1 5DE29A68
P 9125 2750
AR Path="/5DC45DB1/5DE29A68" Ref="K?"  Part="1" 
AR Path="/5E07C1CD/5DE29A68" Ref="K12"  Part="1" 
F 0 "K12" V 9171 2520 50  0000 R CNN
F 1 "1461402-6" V 9080 2520 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 9125 2750 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 9125 2750 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 9125 2750 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 9125 2750 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 9125 2750 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 9125 2750 50  0001 L BNN "Codigo Digikey"
	1    9125 2750
	0    -1   -1   0   
$EndComp
$Comp
L 1461402-6:1461402-6 K?
U 1 1 5DE2F430
P 6550 4100
AR Path="/5DC45DB1/5DE2F430" Ref="K?"  Part="1" 
AR Path="/5E07C1CD/5DE2F430" Ref="K13"  Part="1" 
F 0 "K13" V 6596 3870 50  0000 R CNN
F 1 "1461402-6" V 6505 3870 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 6550 4100 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 6550 4100 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 6550 4100 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 6550 4100 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 6550 4100 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 6550 4100 50  0001 L BNN "Codigo Digikey"
	1    6550 4100
	0    -1   -1   0   
$EndComp
$Comp
L 1461402-6:1461402-6 K?
U 1 1 5DE34C1C
P 9125 4125
AR Path="/5DC45DB1/5DE34C1C" Ref="K?"  Part="1" 
AR Path="/5E07C1CD/5DE34C1C" Ref="K14"  Part="1" 
F 0 "K14" V 9171 3895 50  0000 R CNN
F 1 "1461402-6" V 9080 3895 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 9125 4125 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 9125 4125 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 9125 4125 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 9125 4125 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 9125 4125 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 9125 4125 50  0001 L BNN "Codigo Digikey"
	1    9125 4125
	0    -1   -1   0   
$EndComp
$Comp
L 1461402-6:1461402-6 K?
U 1 1 5DE3A5B5
P 6550 5475
AR Path="/5DC45DB1/5DE3A5B5" Ref="K?"  Part="1" 
AR Path="/5E07C1CD/5DE3A5B5" Ref="K15"  Part="1" 
F 0 "K15" V 6596 5245 50  0000 R CNN
F 1 "1461402-6" V 6505 5245 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 6550 5475 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 6550 5475 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 6550 5475 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 6550 5475 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 6550 5475 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 6550 5475 50  0001 L BNN "Codigo Digikey"
	1    6550 5475
	0    -1   -1   0   
$EndComp
$Comp
L 1461402-6:1461402-6 K?
U 1 1 5DE3FD95
P 9125 5500
AR Path="/5DC45DB1/5DE3FD95" Ref="K?"  Part="1" 
AR Path="/5E07C1CD/5DE3FD95" Ref="K16"  Part="1" 
F 0 "K16" V 9171 5270 50  0000 R CNN
F 1 "1461402-6" V 9080 5270 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 9125 5500 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 9125 5500 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 9125 5500 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 9125 5500 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 9125 5500 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 9125 5500 50  0001 L BNN "Codigo Digikey"
	1    9125 5500
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D12
U 1 1 5DE148D0
P 3800 3625
F 0 "D12" H 3792 3370 50  0000 C CNN
F 1 "LED" H 3792 3461 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 3625 50  0001 C CNN
F 3 "~" H 3800 3625 50  0001 C CNN
	1    3800 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 3625 3550 3625
$Comp
L Device:LED D11
U 1 1 5DE1B896
P 3800 3075
F 0 "D11" H 3792 2820 50  0000 C CNN
F 1 "LED" H 3792 2911 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 3075 50  0001 C CNN
F 3 "~" H 3800 3075 50  0001 C CNN
	1    3800 3075
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 3075 3550 3075
$Comp
L Device:LED D10
U 1 1 5DE21405
P 3800 2500
F 0 "D10" H 3792 2245 50  0000 C CNN
F 1 "LED" H 3792 2336 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 2500 50  0001 C CNN
F 3 "~" H 3800 2500 50  0001 C CNN
	1    3800 2500
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 2500 3550 2500
$Comp
L Device:LED D9
U 1 1 5DE27183
P 3800 1950
F 0 "D9" H 3792 1695 50  0000 C CNN
F 1 "LED" H 3792 1786 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 1950 50  0001 C CNN
F 3 "~" H 3800 1950 50  0001 C CNN
	1    3800 1950
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 1950 3550 1950
$Comp
L Device:LED D13
U 1 1 5DE2F0CD
P 3800 4200
F 0 "D13" H 3792 3945 50  0000 C CNN
F 1 "LED" H 3792 4036 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 4200 50  0001 C CNN
F 3 "~" H 3800 4200 50  0001 C CNN
	1    3800 4200
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 4200 3550 4200
$Comp
L Device:LED D14
U 1 1 5DE347B7
P 3800 4750
F 0 "D14" H 3792 4495 50  0000 C CNN
F 1 "LED" H 3792 4586 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 4750 50  0001 C CNN
F 3 "~" H 3800 4750 50  0001 C CNN
	1    3800 4750
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 4750 3550 4750
$Comp
L Device:LED D15
U 1 1 5DE3A49D
P 3800 5325
F 0 "D15" H 3792 5070 50  0000 C CNN
F 1 "LED" H 3792 5161 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 5325 50  0001 C CNN
F 3 "~" H 3800 5325 50  0001 C CNN
	1    3800 5325
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 5325 3550 5325
$Comp
L Device:LED D16
U 1 1 5DE402DE
P 3800 5875
F 0 "D16" H 3792 5620 50  0000 C CNN
F 1 "LED" H 3792 5711 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 5875 50  0001 C CNN
F 3 "~" H 3800 5875 50  0001 C CNN
	1    3800 5875
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 5875 3550 5875
Wire Wire Line
	4250 5875 3950 5875
Wire Wire Line
	3950 5325 4150 5325
Wire Wire Line
	3950 4750 4050 4750
Wire Wire Line
	3950 3075 4050 3075
Wire Wire Line
	3950 2500 4150 2500
Wire Wire Line
	3950 1950 4250 1950
$EndSCHEMATC
