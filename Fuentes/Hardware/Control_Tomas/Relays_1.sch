EESchema Schematic File Version 4
LIBS:Control_Tomas-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+12V #PWR?
U 1 1 5DC46103
P 6350 825
AR Path="/5DC46103" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC46103" Ref="#PWR018"  Part="1" 
F 0 "#PWR018" H 6350 675 50  0001 C CNN
F 1 "+12V" H 6365 998 50  0000 C CNN
F 2 "" H 6350 825 50  0001 C CNN
F 3 "" H 6350 825 50  0001 C CNN
	1    6350 825 
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR?
U 1 1 5DC4610B
P 4750 4500
AR Path="/5DC4610B" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4610B" Ref="#PWR037"  Part="1" 
F 0 "#PWR037" H 4750 4250 50  0001 C CNN
F 1 "Earth" H 4750 4350 50  0001 C CNN
F 2 "" H 4750 4500 50  0001 C CNN
F 3 "~" H 4750 4500 50  0001 C CNN
	1    4750 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 4500 4750 4450
$Comp
L power:+12V #PWR?
U 1 1 5DC46118
P 5225 3200
AR Path="/5DC46118" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC46118" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 5225 3050 50  0001 C CNN
F 1 "+12V" H 5240 3373 50  0000 C CNN
F 2 "" H 5225 3200 50  0001 C CNN
F 3 "" H 5225 3200 50  0001 C CNN
	1    5225 3200
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 1 1 5DC4751B
P 2700 1850
AR Path="/5DC4751B" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DC4751B" Ref="U2"  Part="1" 
F 0 "U2" H 2700 2175 50  0000 C CNN
F 1 "TLP291-4" H 2700 2084 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 1650 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 1850 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 1850 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 1850 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 1850 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 1850 50  0001 C CNN "Codigo Digikey"
	1    2700 1850
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5DC47521
P 1800 1675
AR Path="/5DC47521" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC47521" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 1800 1525 50  0001 C CNN
F 1 "+3.3V" H 1815 1848 50  0000 C CNN
F 2 "" H 1800 1675 50  0001 C CNN
F 3 "" H 1800 1675 50  0001 C CNN
	1    1800 1675
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DC47527
P 3400 1950
AR Path="/5DC47527" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC47527" Ref="R6"  Part="1" 
F 0 "R6" V 3193 1950 50  0000 C CNN
F 1 "470" V 3284 1950 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 1950 50  0001 C CNN
F 3 "~" H 3400 1950 50  0001 C CNN
	1    3400 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 1950 3250 1950
$Comp
L power:+12V #PWR?
U 1 1 5DC4752F
P 3100 1675
AR Path="/5DC4752F" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4752F" Ref="#PWR021"  Part="1" 
F 0 "#PWR021" H 3100 1525 50  0001 C CNN
F 1 "+12V" H 3115 1848 50  0000 C CNN
F 2 "" H 3100 1675 50  0001 C CNN
F 3 "" H 3100 1675 50  0001 C CNN
	1    3100 1675
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 1750 3100 1750
$Comp
L Device:R R?
U 1 1 5DCD225A
P 2100 1750
AR Path="/5DCD225A" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD225A" Ref="R5"  Part="1" 
F 0 "R5" V 2307 1750 50  0000 C CNN
F 1 "470" V 2216 1750 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 1750 50  0001 C CNN
F 3 "~" H 2100 1750 50  0001 C CNN
	1    2100 1750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 1750 2250 1750
Wire Wire Line
	1800 1675 1800 1750
Text HLabel 1650 1950 0    50   Input ~ 0
IN_1
Wire Wire Line
	1950 1750 1800 1750
Wire Wire Line
	3100 1750 3100 1675
Wire Wire Line
	2400 1950 1650 1950
$Comp
L power:+3.3V #PWR?
U 1 1 5DC4A646
P 1800 2225
AR Path="/5DC4A646" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4A646" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 1800 2075 50  0001 C CNN
F 1 "+3.3V" H 1815 2398 50  0000 C CNN
F 2 "" H 1800 2225 50  0001 C CNN
F 3 "" H 1800 2225 50  0001 C CNN
	1    1800 2225
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DC4A64C
P 3400 2500
AR Path="/5DC4A64C" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC4A64C" Ref="R8"  Part="1" 
F 0 "R8" V 3193 2500 50  0000 C CNN
F 1 "470" V 3284 2500 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 2500 50  0001 C CNN
F 3 "~" H 3400 2500 50  0001 C CNN
	1    3400 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 2500 3250 2500
$Comp
L power:+12V #PWR?
U 1 1 5DC4A654
P 3100 2225
AR Path="/5DC4A654" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4A654" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 3100 2075 50  0001 C CNN
F 1 "+12V" H 3115 2398 50  0000 C CNN
F 2 "" H 3100 2225 50  0001 C CNN
F 3 "" H 3100 2225 50  0001 C CNN
	1    3100 2225
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2300 3100 2300
$Comp
L Device:R R?
U 1 1 5DCD225F
P 2100 2300
AR Path="/5DCD225F" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD225F" Ref="R7"  Part="1" 
F 0 "R7" V 2307 2300 50  0000 C CNN
F 1 "470" V 2216 2300 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 2300 50  0001 C CNN
F 3 "~" H 2100 2300 50  0001 C CNN
	1    2100 2300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 2300 2250 2300
Wire Wire Line
	1800 2225 1800 2300
Text HLabel 1650 2500 0    50   Input ~ 0
IN_2
Wire Wire Line
	1950 2300 1800 2300
Wire Wire Line
	3100 2300 3100 2225
Wire Wire Line
	2400 2500 1650 2500
$Comp
L power:+3.3V #PWR?
U 1 1 5DCD2261
P 1800 2800
AR Path="/5DCD2261" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD2261" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 1800 2650 50  0001 C CNN
F 1 "+3.3V" H 1815 2973 50  0000 C CNN
F 2 "" H 1800 2800 50  0001 C CNN
F 3 "" H 1800 2800 50  0001 C CNN
	1    1800 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DC4BED9
P 3400 3075
AR Path="/5DC4BED9" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC4BED9" Ref="R10"  Part="1" 
F 0 "R10" V 3193 3075 50  0000 C CNN
F 1 "470" V 3284 3075 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 3075 50  0001 C CNN
F 3 "~" H 3400 3075 50  0001 C CNN
	1    3400 3075
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 3075 3250 3075
$Comp
L power:+12V #PWR?
U 1 1 5DC4BEE1
P 3100 2800
AR Path="/5DC4BEE1" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4BEE1" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 3100 2650 50  0001 C CNN
F 1 "+12V" H 3115 2973 50  0000 C CNN
F 2 "" H 3100 2800 50  0001 C CNN
F 3 "" H 3100 2800 50  0001 C CNN
	1    3100 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2875 3100 2875
$Comp
L Device:R R?
U 1 1 5DC4BEE8
P 2100 2875
AR Path="/5DC4BEE8" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC4BEE8" Ref="R9"  Part="1" 
F 0 "R9" V 2307 2875 50  0000 C CNN
F 1 "470" V 2216 2875 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 2875 50  0001 C CNN
F 3 "~" H 2100 2875 50  0001 C CNN
	1    2100 2875
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 2875 2250 2875
Wire Wire Line
	1800 2800 1800 2875
Text HLabel 1650 3075 0    50   Input ~ 0
IN_3
Wire Wire Line
	1950 2875 1800 2875
Wire Wire Line
	3100 2875 3100 2800
Wire Wire Line
	2400 3075 1650 3075
$Comp
L power:+3.3V #PWR?
U 1 1 5DC4BEFA
P 1800 3350
AR Path="/5DC4BEFA" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4BEFA" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 1800 3200 50  0001 C CNN
F 1 "+3.3V" H 1815 3523 50  0000 C CNN
F 2 "" H 1800 3350 50  0001 C CNN
F 3 "" H 1800 3350 50  0001 C CNN
	1    1800 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DCD2267
P 3400 3625
AR Path="/5DCD2267" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD2267" Ref="R12"  Part="1" 
F 0 "R12" V 3193 3625 50  0000 C CNN
F 1 "470" V 3284 3625 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 3625 50  0001 C CNN
F 3 "~" H 3400 3625 50  0001 C CNN
	1    3400 3625
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 3625 3250 3625
$Comp
L power:+12V #PWR?
U 1 1 5DC4BF08
P 3100 3350
AR Path="/5DC4BF08" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4BF08" Ref="#PWR030"  Part="1" 
F 0 "#PWR030" H 3100 3200 50  0001 C CNN
F 1 "+12V" H 3115 3523 50  0000 C CNN
F 2 "" H 3100 3350 50  0001 C CNN
F 3 "" H 3100 3350 50  0001 C CNN
	1    3100 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 3425 3100 3425
$Comp
L Device:R R?
U 1 1 5DC4BF0F
P 2100 3425
AR Path="/5DC4BF0F" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC4BF0F" Ref="R11"  Part="1" 
F 0 "R11" V 2307 3425 50  0000 C CNN
F 1 "470" V 2216 3425 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 3425 50  0001 C CNN
F 3 "~" H 2100 3425 50  0001 C CNN
	1    2100 3425
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 3425 2250 3425
Wire Wire Line
	1800 3350 1800 3425
Text HLabel 1650 3625 0    50   Input ~ 0
IN_4
Wire Wire Line
	1950 3425 1800 3425
Wire Wire Line
	3100 3425 3100 3350
Wire Wire Line
	2400 3625 1650 3625
$Comp
L power:+3.3V #PWR?
U 1 1 5DC4CF94
P 1800 3925
AR Path="/5DC4CF94" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4CF94" Ref="#PWR033"  Part="1" 
F 0 "#PWR033" H 1800 3775 50  0001 C CNN
F 1 "+3.3V" H 1815 4098 50  0000 C CNN
F 2 "" H 1800 3925 50  0001 C CNN
F 3 "" H 1800 3925 50  0001 C CNN
	1    1800 3925
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DC4CF9A
P 3400 4200
AR Path="/5DC4CF9A" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC4CF9A" Ref="R14"  Part="1" 
F 0 "R14" V 3193 4200 50  0000 C CNN
F 1 "470" V 3284 4200 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 4200 50  0001 C CNN
F 3 "~" H 3400 4200 50  0001 C CNN
	1    3400 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 4200 3250 4200
$Comp
L power:+12V #PWR?
U 1 1 5DCD226D
P 3100 3925
AR Path="/5DCD226D" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD226D" Ref="#PWR034"  Part="1" 
F 0 "#PWR034" H 3100 3775 50  0001 C CNN
F 1 "+12V" H 3115 4098 50  0000 C CNN
F 2 "" H 3100 3925 50  0001 C CNN
F 3 "" H 3100 3925 50  0001 C CNN
	1    3100 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 4000 3100 4000
$Comp
L Device:R R?
U 1 1 5DCD226E
P 2100 4000
AR Path="/5DCD226E" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD226E" Ref="R13"  Part="1" 
F 0 "R13" V 2307 4000 50  0000 C CNN
F 1 "470" V 2216 4000 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 4000 50  0001 C CNN
F 3 "~" H 2100 4000 50  0001 C CNN
	1    2100 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 4000 2250 4000
Wire Wire Line
	1800 3925 1800 4000
Text HLabel 1650 4200 0    50   Input ~ 0
IN_5
Wire Wire Line
	1950 4000 1800 4000
Wire Wire Line
	3100 4000 3100 3925
Wire Wire Line
	2400 4200 1650 4200
$Comp
L power:+3.3V #PWR?
U 1 1 5DCD2270
P 1800 4475
AR Path="/5DCD2270" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD2270" Ref="#PWR035"  Part="1" 
F 0 "#PWR035" H 1800 4325 50  0001 C CNN
F 1 "+3.3V" H 1815 4648 50  0000 C CNN
F 2 "" H 1800 4475 50  0001 C CNN
F 3 "" H 1800 4475 50  0001 C CNN
	1    1800 4475
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DC4CFC1
P 3400 4750
AR Path="/5DC4CFC1" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC4CFC1" Ref="R16"  Part="1" 
F 0 "R16" V 3193 4750 50  0000 C CNN
F 1 "470" V 3284 4750 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 4750 50  0001 C CNN
F 3 "~" H 3400 4750 50  0001 C CNN
	1    3400 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 4750 3250 4750
$Comp
L power:+12V #PWR?
U 1 1 5DC4CFC9
P 3100 4475
AR Path="/5DC4CFC9" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4CFC9" Ref="#PWR036"  Part="1" 
F 0 "#PWR036" H 3100 4325 50  0001 C CNN
F 1 "+12V" H 3115 4648 50  0000 C CNN
F 2 "" H 3100 4475 50  0001 C CNN
F 3 "" H 3100 4475 50  0001 C CNN
	1    3100 4475
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 4550 3100 4550
$Comp
L Device:R R?
U 1 1 5DCD2273
P 2100 4550
AR Path="/5DCD2273" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD2273" Ref="R15"  Part="1" 
F 0 "R15" V 2307 4550 50  0000 C CNN
F 1 "470" V 2216 4550 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 4550 50  0001 C CNN
F 3 "~" H 2100 4550 50  0001 C CNN
	1    2100 4550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 4550 2250 4550
Wire Wire Line
	1800 4475 1800 4550
Text HLabel 1650 4750 0    50   Input ~ 0
IN_6
Wire Wire Line
	1950 4550 1800 4550
Wire Wire Line
	3100 4550 3100 4475
Wire Wire Line
	2400 4750 1650 4750
$Comp
L power:+3.3V #PWR?
U 1 1 5DCD2275
P 1800 5050
AR Path="/5DCD2275" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD2275" Ref="#PWR040"  Part="1" 
F 0 "#PWR040" H 1800 4900 50  0001 C CNN
F 1 "+3.3V" H 1815 5223 50  0000 C CNN
F 2 "" H 1800 5050 50  0001 C CNN
F 3 "" H 1800 5050 50  0001 C CNN
	1    1800 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DCD2276
P 3400 5325
AR Path="/5DCD2276" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD2276" Ref="R18"  Part="1" 
F 0 "R18" V 3193 5325 50  0000 C CNN
F 1 "470" V 3284 5325 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 5325 50  0001 C CNN
F 3 "~" H 3400 5325 50  0001 C CNN
	1    3400 5325
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 5325 3250 5325
$Comp
L power:+12V #PWR?
U 1 1 5DCD2277
P 3100 5050
AR Path="/5DCD2277" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD2277" Ref="#PWR041"  Part="1" 
F 0 "#PWR041" H 3100 4900 50  0001 C CNN
F 1 "+12V" H 3115 5223 50  0000 C CNN
F 2 "" H 3100 5050 50  0001 C CNN
F 3 "" H 3100 5050 50  0001 C CNN
	1    3100 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 5125 3100 5125
$Comp
L Device:R R?
U 1 1 5DCD2278
P 2100 5125
AR Path="/5DCD2278" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD2278" Ref="R17"  Part="1" 
F 0 "R17" V 2307 5125 50  0000 C CNN
F 1 "470" V 2216 5125 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 5125 50  0001 C CNN
F 3 "~" H 2100 5125 50  0001 C CNN
	1    2100 5125
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 5125 2250 5125
Wire Wire Line
	1800 5050 1800 5125
Text HLabel 1650 5325 0    50   Input ~ 0
IN_7
Wire Wire Line
	1950 5125 1800 5125
Wire Wire Line
	3100 5125 3100 5050
Wire Wire Line
	2400 5325 1650 5325
$Comp
L power:+3.3V #PWR?
U 1 1 5DCD227A
P 1800 5600
AR Path="/5DCD227A" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DCD227A" Ref="#PWR042"  Part="1" 
F 0 "#PWR042" H 1800 5450 50  0001 C CNN
F 1 "+3.3V" H 1815 5773 50  0000 C CNN
F 2 "" H 1800 5600 50  0001 C CNN
F 3 "" H 1800 5600 50  0001 C CNN
	1    1800 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DCD227B
P 3400 5875
AR Path="/5DCD227B" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DCD227B" Ref="R20"  Part="1" 
F 0 "R20" V 3193 5875 50  0000 C CNN
F 1 "470" V 3284 5875 50  0000 C CNN
F 2 "lib_fp:R_0603" V 3330 5875 50  0001 C CNN
F 3 "~" H 3400 5875 50  0001 C CNN
	1    3400 5875
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 5875 3250 5875
$Comp
L power:+12V #PWR?
U 1 1 5DC4D017
P 3100 5600
AR Path="/5DC4D017" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DC4D017" Ref="#PWR043"  Part="1" 
F 0 "#PWR043" H 3100 5450 50  0001 C CNN
F 1 "+12V" H 3115 5773 50  0000 C CNN
F 2 "" H 3100 5600 50  0001 C CNN
F 3 "" H 3100 5600 50  0001 C CNN
	1    3100 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 5675 3100 5675
$Comp
L Device:R R?
U 1 1 5DC4D01E
P 2100 5675
AR Path="/5DC4D01E" Ref="R?"  Part="1" 
AR Path="/5DC45DB1/5DC4D01E" Ref="R19"  Part="1" 
F 0 "R19" V 2307 5675 50  0000 C CNN
F 1 "470" V 2216 5675 50  0000 C CNN
F 2 "lib_fp:R_0603" V 2030 5675 50  0001 C CNN
F 3 "~" H 2100 5675 50  0001 C CNN
	1    2100 5675
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 5675 2250 5675
Wire Wire Line
	1800 5600 1800 5675
Text HLabel 1650 5875 0    50   Input ~ 0
IN_8
Wire Wire Line
	1950 5675 1800 5675
Wire Wire Line
	3100 5675 3100 5600
Wire Wire Line
	2400 5875 1650 5875
Wire Wire Line
	4350 3550 4250 3550
Wire Wire Line
	4250 3550 4250 1950
Wire Wire Line
	4350 3650 4150 3650
Wire Wire Line
	4150 3650 4150 2500
Wire Wire Line
	4050 3750 4050 3075
Wire Wire Line
	4050 3750 4350 3750
Wire Wire Line
	4350 3850 3950 3850
Wire Wire Line
	3950 3850 3950 3625
Wire Wire Line
	4350 3950 3950 3950
Wire Wire Line
	3950 3950 3950 4200
Wire Wire Line
	4350 4050 4050 4050
Wire Wire Line
	4050 4050 4050 4750
Wire Wire Line
	4350 4150 4150 4150
Wire Wire Line
	4150 4150 4150 5325
Wire Wire Line
	4350 4250 4250 4250
Wire Wire Line
	4250 4250 4250 5875
Wire Wire Line
	5225 3450 5225 3200
Wire Wire Line
	5150 3450 5225 3450
Wire Wire Line
	6350 850  6350 825 
Text Label 5250 3550 0    50   ~ 0
R1
Text Label 5250 3650 0    50   ~ 0
R2
Text Label 5250 3750 0    50   ~ 0
R3
Text Label 5250 3850 0    50   ~ 0
R4
Text Label 5250 3950 0    50   ~ 0
R5
Text Label 5250 4050 0    50   ~ 0
R6
Text Label 5250 4150 0    50   ~ 0
R7
Text Label 5250 4250 0    50   ~ 0
R8
Wire Wire Line
	6350 1875 6350 1850
Wire Wire Line
	5150 3550 5400 3550
Wire Wire Line
	5150 3650 5475 3650
Wire Wire Line
	6650 850  6650 825 
$Comp
L Connector:Screw_Terminal_01x01 J4
U 1 1 5E03C695
P 7625 1850
F 0 "J4" H 7705 1892 50  0000 L CNN
F 1 "Toma01" H 7705 1801 50  0000 L CNN
F 2 "lib_fp:1704994" H 7625 1850 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 7625 1850 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 7625 1850 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 7625 1850 50  0001 C CNN "Fabricante"
F 6 "1704994" H 7625 1850 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 7625 1850 50  0001 C CNN "Codigo Digikey"
	1    7625 1850
	1    0    0    -1  
$EndComp
Text Label 7125 1850 0    50   ~ 0
Toma1
Text GLabel 6725 825  2    50   BiDi ~ 0
VPOW
$Comp
L Device:Polyfuse F1
U 1 1 5DD9DB33
P 6950 1850
F 0 "F1" V 6725 1850 50  0000 C CNN
F 1 "Polyfuse" V 6816 1850 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 7000 1650 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 6950 1850 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 6950 1850 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 6950 1850 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 6950 1850 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 6950 1850 50  0001 C CNN "Codigo Digikey"
	1    6950 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	7100 1850 7425 1850
Wire Wire Line
	6800 1850 6650 1850
$Comp
L 1461402-6:1461402-6 K1
U 1 1 5DE19DAB
P 6550 1350
F 0 "K1" V 6596 1120 50  0000 R CNN
F 1 "1461402-6" V 6505 1120 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 6550 1350 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 6550 1350 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 6550 1350 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 6550 1350 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 6550 1350 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 6550 1350 50  0001 L BNN "Codigo Digikey"
	1    6550 1350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6725 825  6650 825 
$Comp
L power:+12V #PWR?
U 1 1 5DE3C812
P 6350 2200
AR Path="/5DE3C812" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DE3C812" Ref="#PWR022"  Part="1" 
F 0 "#PWR022" H 6350 2050 50  0001 C CNN
F 1 "+12V" H 6365 2373 50  0000 C CNN
F 2 "" H 6350 2200 50  0001 C CNN
F 3 "" H 6350 2200 50  0001 C CNN
	1    6350 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2225 6350 2200
Wire Wire Line
	6350 3250 6350 3225
Wire Wire Line
	6650 2225 6650 2200
Text Label 7125 3225 0    50   ~ 0
Toma3
Text GLabel 6725 2200 2    50   BiDi ~ 0
VPOW
Wire Wire Line
	7100 3225 7425 3225
Wire Wire Line
	6800 3225 6650 3225
Wire Wire Line
	6725 2200 6650 2200
$Comp
L power:+12V #PWR?
U 1 1 5DE3FD3A
P 6350 3575
AR Path="/5DE3FD3A" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DE3FD3A" Ref="#PWR031"  Part="1" 
F 0 "#PWR031" H 6350 3425 50  0001 C CNN
F 1 "+12V" H 6365 3748 50  0000 C CNN
F 2 "" H 6350 3575 50  0001 C CNN
F 3 "" H 6350 3575 50  0001 C CNN
	1    6350 3575
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3600 6350 3575
Wire Wire Line
	6350 4625 6350 4600
Wire Wire Line
	6650 3600 6650 3575
Text Label 7125 4600 0    50   ~ 0
Toma5
Text GLabel 6725 3575 2    50   BiDi ~ 0
VPOW
Wire Wire Line
	7100 4600 7425 4600
Wire Wire Line
	6800 4600 6650 4600
Wire Wire Line
	6725 3575 6650 3575
$Comp
L power:+12V #PWR?
U 1 1 5DE433B9
P 6350 4950
AR Path="/5DE433B9" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DE433B9" Ref="#PWR038"  Part="1" 
F 0 "#PWR038" H 6350 4800 50  0001 C CNN
F 1 "+12V" H 6365 5123 50  0000 C CNN
F 2 "" H 6350 4950 50  0001 C CNN
F 3 "" H 6350 4950 50  0001 C CNN
	1    6350 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 4975 6350 4950
Wire Wire Line
	6350 6000 6350 5975
Wire Wire Line
	6650 4975 6650 4950
Text Label 7125 5975 0    50   ~ 0
Toma7
Text GLabel 6725 4950 2    50   BiDi ~ 0
VPOW
Wire Wire Line
	7100 5975 7425 5975
Wire Wire Line
	6800 5975 6650 5975
Wire Wire Line
	6725 4950 6650 4950
Wire Wire Line
	5400 1875 5400 3550
Wire Wire Line
	5400 1875 6350 1875
$Comp
L power:+12V #PWR?
U 1 1 5DE58FE5
P 8925 825
AR Path="/5DE58FE5" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DE58FE5" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 8925 675 50  0001 C CNN
F 1 "+12V" H 8940 998 50  0000 C CNN
F 2 "" H 8925 825 50  0001 C CNN
F 3 "" H 8925 825 50  0001 C CNN
	1    8925 825 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8925 850  8925 825 
Wire Wire Line
	9225 850  9225 825 
Text Label 9700 1850 0    50   ~ 0
Toma2
Text GLabel 9300 825  2    50   BiDi ~ 0
VPOW
Wire Wire Line
	9675 1850 10000 1850
Wire Wire Line
	9375 1850 9225 1850
Wire Wire Line
	9300 825  9225 825 
$Comp
L power:+12V #PWR?
U 1 1 5DE5900F
P 8925 2200
AR Path="/5DE5900F" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DE5900F" Ref="#PWR023"  Part="1" 
F 0 "#PWR023" H 8925 2050 50  0001 C CNN
F 1 "+12V" H 8940 2373 50  0000 C CNN
F 2 "" H 8925 2200 50  0001 C CNN
F 3 "" H 8925 2200 50  0001 C CNN
	1    8925 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8925 2225 8925 2200
Wire Wire Line
	9225 2225 9225 2200
Text Label 9700 3225 0    50   ~ 0
Toma4
Text GLabel 9300 2200 2    50   BiDi ~ 0
VPOW
Wire Wire Line
	9675 3225 10000 3225
Wire Wire Line
	9375 3225 9225 3225
Wire Wire Line
	9300 2200 9225 2200
$Comp
L power:+12V #PWR?
U 1 1 5DE59039
P 8925 3575
AR Path="/5DE59039" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DE59039" Ref="#PWR032"  Part="1" 
F 0 "#PWR032" H 8925 3425 50  0001 C CNN
F 1 "+12V" H 8940 3748 50  0000 C CNN
F 2 "" H 8925 3575 50  0001 C CNN
F 3 "" H 8925 3575 50  0001 C CNN
	1    8925 3575
	1    0    0    -1  
$EndComp
Wire Wire Line
	8925 3600 8925 3575
Wire Wire Line
	9225 3600 9225 3575
Text Label 9700 4600 0    50   ~ 0
Toma6
Text GLabel 9300 3575 2    50   BiDi ~ 0
VPOW
Wire Wire Line
	9675 4600 10000 4600
Wire Wire Line
	9375 4600 9225 4600
Wire Wire Line
	9300 3575 9225 3575
$Comp
L power:+12V #PWR?
U 1 1 5DE59063
P 8925 4950
AR Path="/5DE59063" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5DE59063" Ref="#PWR039"  Part="1" 
F 0 "#PWR039" H 8925 4800 50  0001 C CNN
F 1 "+12V" H 8940 5123 50  0000 C CNN
F 2 "" H 8925 4950 50  0001 C CNN
F 3 "" H 8925 4950 50  0001 C CNN
	1    8925 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8925 4975 8925 4950
Wire Wire Line
	9225 4975 9225 4950
Text Label 9700 5975 0    50   ~ 0
Toma8
Text GLabel 9300 4950 2    50   BiDi ~ 0
VPOW
Wire Wire Line
	9675 5975 10000 5975
Wire Wire Line
	9375 5975 9225 5975
Wire Wire Line
	9300 4950 9225 4950
Wire Wire Line
	5475 1950 8925 1950
Wire Wire Line
	8925 1850 8925 1950
Wire Wire Line
	5150 3750 5550 3750
Wire Wire Line
	5550 3750 5550 3250
Wire Wire Line
	5550 3250 6350 3250
Wire Wire Line
	5150 3850 5625 3850
Wire Wire Line
	5625 3850 5625 3325
Wire Wire Line
	5625 3325 8925 3325
Wire Wire Line
	8925 3225 8925 3325
Wire Wire Line
	5150 3950 5625 3950
Wire Wire Line
	5625 3950 5625 4625
Wire Wire Line
	5625 4625 6350 4625
Wire Wire Line
	5150 4050 5550 4050
Wire Wire Line
	5550 4050 5550 4700
Wire Wire Line
	5550 4700 8925 4700
Wire Wire Line
	8925 4600 8925 4700
Wire Wire Line
	5150 4150 5475 4150
Wire Wire Line
	5475 4150 5475 6000
Wire Wire Line
	5475 6000 6350 6000
Wire Wire Line
	5150 4250 5400 4250
Wire Wire Line
	5400 4250 5400 6075
Wire Wire Line
	5400 6075 8925 6075
Wire Wire Line
	8925 5975 8925 6075
Wire Wire Line
	5475 1950 5475 3650
$Comp
L Device:Polyfuse F2
U 1 1 5DDED6DA
P 9525 1850
F 0 "F2" V 9300 1850 50  0000 C CNN
F 1 "Polyfuse" V 9391 1850 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 9575 1650 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 9525 1850 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 9525 1850 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 9525 1850 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 9525 1850 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 9525 1850 50  0001 C CNN "Codigo Digikey"
	1    9525 1850
	0    1    1    0   
$EndComp
$Comp
L Device:Polyfuse F3
U 1 1 5DDF367E
P 6950 3225
F 0 "F3" V 6725 3225 50  0000 C CNN
F 1 "Polyfuse" V 6816 3225 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 7000 3025 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 6950 3225 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 6950 3225 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 6950 3225 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 6950 3225 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 6950 3225 50  0001 C CNN "Codigo Digikey"
	1    6950 3225
	0    1    1    0   
$EndComp
$Comp
L Device:Polyfuse F4
U 1 1 5DDF8E76
P 9525 3225
F 0 "F4" V 9300 3225 50  0000 C CNN
F 1 "Polyfuse" V 9391 3225 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 9575 3025 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 9525 3225 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 9525 3225 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 9525 3225 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 9525 3225 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 9525 3225 50  0001 C CNN "Codigo Digikey"
	1    9525 3225
	0    1    1    0   
$EndComp
$Comp
L Device:Polyfuse F5
U 1 1 5DDFE67B
P 6950 4600
F 0 "F5" V 6725 4600 50  0000 C CNN
F 1 "Polyfuse" V 6816 4600 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 7000 4400 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 6950 4600 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 6950 4600 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 6950 4600 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 6950 4600 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 6950 4600 50  0001 C CNN "Codigo Digikey"
	1    6950 4600
	0    1    1    0   
$EndComp
$Comp
L Device:Polyfuse F6
U 1 1 5DE03E55
P 9525 4600
F 0 "F6" V 9300 4600 50  0000 C CNN
F 1 "Polyfuse" V 9391 4600 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 9575 4400 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 9525 4600 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 9525 4600 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 9525 4600 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 9525 4600 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 9525 4600 50  0001 C CNN "Codigo Digikey"
	1    9525 4600
	0    1    1    0   
$EndComp
$Comp
L Device:Polyfuse F7
U 1 1 5DE0965E
P 6950 5975
F 0 "F7" V 6725 5975 50  0000 C CNN
F 1 "Polyfuse" V 6816 5975 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 7000 5775 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 6950 5975 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 6950 5975 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 6950 5975 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 6950 5975 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 6950 5975 50  0001 C CNN "Codigo Digikey"
	1    6950 5975
	0    1    1    0   
$EndComp
$Comp
L Device:Polyfuse F8
U 1 1 5DE0EE2A
P 9525 5975
F 0 "F8" V 9300 5975 50  0000 C CNN
F 1 "Polyfuse" V 9391 5975 50  0000 C CNN
F 2 "lib_fp:LVR200S-240" H 9575 5775 50  0001 L CNN
F 3 "https://www.littelfuse.com/~/media/electronics/product_specifications/resettable_ptcs/littelfuse_ptc_lvr200s_240_product_specification.pdf.pdf" H 9525 5975 50  0001 C CNN
F 4 "PTC RESET FUSE 240V 2A RADIAL" V 9525 5975 50  0001 C CNN "Descripcion"
F 5 "Littelfuse Inc." V 9525 5975 50  0001 C CNN "Fabricante"
F 6 "LVR200S-240" V 9525 5975 50  0001 C CNN "Codigo Fabricante"
F 7 "LVR200S-240-ND" V 9525 5975 50  0001 C CNN "Codigo Digikey"
	1    9525 5975
	0    1    1    0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J5
U 1 1 5DE49F40
P 10200 1850
F 0 "J5" H 10280 1892 50  0000 L CNN
F 1 "Toma02" H 10280 1801 50  0000 L CNN
F 2 "lib_fp:1704994" H 10200 1850 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 10200 1850 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 10200 1850 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 10200 1850 50  0001 C CNN "Fabricante"
F 6 "1704994" H 10200 1850 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 10200 1850 50  0001 C CNN "Codigo Digikey"
	1    10200 1850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J6
U 1 1 5DE4F71D
P 7625 3225
F 0 "J6" H 7705 3267 50  0000 L CNN
F 1 "Toma03" H 7705 3176 50  0000 L CNN
F 2 "lib_fp:1704994" H 7625 3225 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 7625 3225 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 7625 3225 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 7625 3225 50  0001 C CNN "Fabricante"
F 6 "1704994" H 7625 3225 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 7625 3225 50  0001 C CNN "Codigo Digikey"
	1    7625 3225
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J7
U 1 1 5DE54EFF
P 10200 3225
F 0 "J7" H 10280 3267 50  0000 L CNN
F 1 "Toma04" H 10280 3176 50  0000 L CNN
F 2 "lib_fp:1704994" H 10200 3225 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 10200 3225 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 10200 3225 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 10200 3225 50  0001 C CNN "Fabricante"
F 6 "1704994" H 10200 3225 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 10200 3225 50  0001 C CNN "Codigo Digikey"
	1    10200 3225
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J8
U 1 1 5DE5AC9E
P 7625 4600
F 0 "J8" H 7705 4642 50  0000 L CNN
F 1 "Toma05" H 7705 4551 50  0000 L CNN
F 2 "lib_fp:1704994" H 7625 4600 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 7625 4600 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 7625 4600 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 7625 4600 50  0001 C CNN "Fabricante"
F 6 "1704994" H 7625 4600 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 7625 4600 50  0001 C CNN "Codigo Digikey"
	1    7625 4600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J9
U 1 1 5DE6047C
P 10200 4600
F 0 "J9" H 10280 4642 50  0000 L CNN
F 1 "Toma06" H 10280 4551 50  0000 L CNN
F 2 "lib_fp:1704994" H 10200 4600 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 10200 4600 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 10200 4600 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 10200 4600 50  0001 C CNN "Fabricante"
F 6 "1704994" H 10200 4600 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 10200 4600 50  0001 C CNN "Codigo Digikey"
	1    10200 4600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J10
U 1 1 5DE66097
P 7625 5975
F 0 "J10" H 7705 6017 50  0000 L CNN
F 1 "Toma07" H 7705 5926 50  0000 L CNN
F 2 "lib_fp:1704994" H 7625 5975 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 7625 5975 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 7625 5975 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 7625 5975 50  0001 C CNN "Fabricante"
F 6 "1704994" H 7625 5975 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 7625 5975 50  0001 C CNN "Codigo Digikey"
	1    7625 5975
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x01 J11
U 1 1 5DE6B871
P 10200 5975
F 0 "J11" H 10280 6017 50  0000 L CNN
F 1 "Toma08" H 10280 5926 50  0000 L CNN
F 2 "lib_fp:1704994" H 10200 5975 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Phoenix%20Contact%20PDFs/1704994.pdf" H 10200 5975 50  0001 C CNN
F 4 "TERM BLOCK 1POS 45DEG PCB" H 10200 5975 50  0001 C CNN "Descripcion"
F 5 "Phoenix Contact" H 10200 5975 50  0001 C CNN "Fabricante"
F 6 "1704994" H 10200 5975 50  0001 C CNN "Codigo Fabricante"
F 7 "277-6835-ND" H 10200 5975 50  0001 C CNN "Codigo Digikey"
	1    10200 5975
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 2 1 5DDC8BBB
P 2700 2400
AR Path="/5DDC8BBB" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDC8BBB" Ref="U2"  Part="2" 
F 0 "U2" H 2700 2725 50  0000 C CNN
F 1 "TLP291-4" H 2700 2634 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 2200 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 2400 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 2400 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 2400 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 2400 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 2400 50  0001 C CNN "Codigo Digikey"
	2    2700 2400
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 3 1 5DDC8C4B
P 2700 2975
AR Path="/5DDC8C4B" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDC8C4B" Ref="U2"  Part="3" 
F 0 "U2" H 2700 3300 50  0000 C CNN
F 1 "TLP291-4" H 2700 3209 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 2775 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 2975 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 2975 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 2975 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 2975 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 2975 50  0001 C CNN "Codigo Digikey"
	3    2700 2975
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 4 1 5DDC8CE1
P 2700 3525
AR Path="/5DDC8CE1" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDC8CE1" Ref="U2"  Part="4" 
F 0 "U2" H 2700 3850 50  0000 C CNN
F 1 "TLP291-4" H 2700 3759 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 3325 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 3525 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 3525 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 3525 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 3525 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 3525 50  0001 C CNN "Codigo Digikey"
	4    2700 3525
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 1 1 5DDD51CF
P 2700 4100
AR Path="/5DDD51CF" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDD51CF" Ref="U4"  Part="1" 
F 0 "U4" H 2700 4425 50  0000 C CNN
F 1 "TLP291-4" H 2700 4334 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 3900 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 4100 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 4100 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 4100 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 4100 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 4100 50  0001 C CNN "Codigo Digikey"
	1    2700 4100
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 2 1 5DDD51DA
P 2700 4650
AR Path="/5DDD51DA" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDD51DA" Ref="U4"  Part="2" 
F 0 "U4" H 2700 4975 50  0000 C CNN
F 1 "TLP291-4" H 2700 4884 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 4450 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 4650 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 4650 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 4650 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 4650 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 4650 50  0001 C CNN "Codigo Digikey"
	2    2700 4650
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 3 1 5DDD51E5
P 2700 5225
AR Path="/5DDD51E5" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDD51E5" Ref="U4"  Part="3" 
F 0 "U4" H 2700 5550 50  0000 C CNN
F 1 "TLP291-4" H 2700 5459 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 5025 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 5225 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 5225 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 5225 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 5225 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 5225 50  0001 C CNN "Codigo Digikey"
	3    2700 5225
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP291-4 U?
U 4 1 5DDD51F0
P 2700 5775
AR Path="/5DDD51F0" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDD51F0" Ref="U4"  Part="4" 
F 0 "U4" H 2700 6100 50  0000 C CNN
F 1 "TLP291-4" H 2700 6009 50  0000 C CNN
F 2 "lib_fp:SO-16-N" H 2500 5575 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12858&prodName=TLP291-4" H 2700 5775 50  0001 L CNN
F 4 "OPTOISOLTR 2.5KV 4CH TRANS 16-SO" H 2700 5775 50  0001 C CNN "Descripcion"
F 5 "Toshiba Semiconductor and Storage" H 2700 5775 50  0001 C CNN "Fabricante"
F 6 "TLP291-4(TP,E)" H 2700 5775 50  0001 C CNN "Codigo Fabricante"
F 7 "TLP291-4(TPE)CT-ND" H 2700 5775 50  0001 C CNN "Codigo Digikey"
	4    2700 5775
	1    0    0    -1  
$EndComp
$Comp
L Transistor_Array:ULN2803A U?
U 1 1 5DDF8727
P 4750 3750
AR Path="/5DDF8727" Ref="U?"  Part="1" 
AR Path="/5DC45DB1/5DDF8727" Ref="U3"  Part="1" 
AR Path="/5E07C1CD/5DDF8727" Ref="U?"  Part="1" 
F 0 "U3" H 4750 4317 50  0000 C CNN
F 1 "ULN2803A" H 4750 4226 50  0000 C CNN
F 2 "lib_fp:DIP-18" H 4800 3100 50  0001 L CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/e4/fa/1c/16/4e/39/49/04/CD00000179.pdf/files/CD00000179.pdf/jcr:content/translations/en.CD00000179.pdf" H 4850 3550 50  0001 C CNN
F 4 "TRANS 8NPN DARL 50V 0.5A 18DIP" H 4750 3750 50  0001 C CNN "Descripcion"
F 5 "STMicroelectronics" H 4750 3750 50  0001 C CNN "Fabricante"
F 6 "ULN2803A" H 4750 3750 50  0001 C CNN "Codigo Fabricante"
F 7 "497-2356-5-ND" H 4750 3750 50  0001 C CNN "Codigo Digikey"
	1    4750 3750
	1    0    0    -1  
$EndComp
$Comp
L 1461402-6:1461402-6 K2
U 1 1 5DDFFBB1
P 9125 1350
F 0 "K2" V 9171 1120 50  0000 R CNN
F 1 "1461402-6" V 9080 1120 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 9125 1350 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 9125 1350 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 9125 1350 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 9125 1350 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 9125 1350 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 9125 1350 50  0001 L BNN "Codigo Digikey"
	1    9125 1350
	0    -1   -1   0   
$EndComp
$Comp
L 1461402-6:1461402-6 K3
U 1 1 5DDFFC75
P 6550 2725
F 0 "K3" V 6596 2495 50  0000 R CNN
F 1 "1461402-6" V 6505 2495 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 6550 2725 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 6550 2725 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 6550 2725 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 6550 2725 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 6550 2725 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 6550 2725 50  0001 L BNN "Codigo Digikey"
	1    6550 2725
	0    -1   -1   0   
$EndComp
$Comp
L 1461402-6:1461402-6 K4
U 1 1 5DDFFD2B
P 9125 2725
F 0 "K4" V 9171 2495 50  0000 R CNN
F 1 "1461402-6" V 9080 2495 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 9125 2725 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 9125 2725 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 9125 2725 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 9125 2725 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 9125 2725 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 9125 2725 50  0001 L BNN "Codigo Digikey"
	1    9125 2725
	0    -1   -1   0   
$EndComp
$Comp
L 1461402-6:1461402-6 K5
U 1 1 5DDFFDF3
P 6550 4100
F 0 "K5" V 6596 3870 50  0000 R CNN
F 1 "1461402-6" V 6505 3870 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 6550 4100 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 6550 4100 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 6550 4100 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 6550 4100 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 6550 4100 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 6550 4100 50  0001 L BNN "Codigo Digikey"
	1    6550 4100
	0    -1   -1   0   
$EndComp
$Comp
L 1461402-6:1461402-6 K6
U 1 1 5DE0005F
P 9125 4100
F 0 "K6" V 9171 3870 50  0000 R CNN
F 1 "1461402-6" V 9080 3870 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 9125 4100 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 9125 4100 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 9125 4100 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 9125 4100 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 9125 4100 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 9125 4100 50  0001 L BNN "Codigo Digikey"
	1    9125 4100
	0    -1   -1   0   
$EndComp
$Comp
L 1461402-6:1461402-6 K7
U 1 1 5DE00372
P 6550 5475
F 0 "K7" V 6596 5245 50  0000 R CNN
F 1 "1461402-6" V 6505 5245 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 6550 5475 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 6550 5475 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 6550 5475 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 6550 5475 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 6550 5475 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 6550 5475 50  0001 L BNN "Codigo Digikey"
	1    6550 5475
	0    -1   -1   0   
$EndComp
$Comp
L 1461402-6:1461402-6 K8
U 1 1 5DE00422
P 9125 5475
F 0 "K8" V 9171 5245 50  0000 R CNN
F 1 "1461402-6" V 9080 5245 50  0000 R CNN
F 2 "lib_fp:RELAY_1461402-6" H 9125 5475 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=OJ_OJE_series_relay_data_sheet_E&DocType=DS&DocLang=English" H 9125 5475 50  0001 L BNN
F 4 "RELAY GEN PURPOSE SPST 10A 12V" H 9125 5475 50  0001 L BNN "Descripcion"
F 5 "TE Connectivity Potter & Brumfield Relays" H 9125 5475 50  0001 L BNN "Fabricante "
F 6 "OJE-SH-112HM,000" H 9125 5475 50  0001 L BNN "Codigo Fabricante"
F 7 "PB876-ND" H 9125 5475 50  0001 L BNN "Codigo Digikey"
	1    9125 5475
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5DE74649
P 3800 1950
AR Path="/5E07C1CD/5DE74649" Ref="D?"  Part="1" 
AR Path="/5DC45DB1/5DE74649" Ref="D1"  Part="1" 
F 0 "D1" H 3792 1695 50  0000 C CNN
F 1 "LED" H 3792 1786 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 1950 50  0001 C CNN
F 3 "~" H 3800 1950 50  0001 C CNN
	1    3800 1950
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 1950 3550 1950
$Comp
L Device:LED D?
U 1 1 5DE79C05
P 3800 2500
AR Path="/5E07C1CD/5DE79C05" Ref="D?"  Part="1" 
AR Path="/5DC45DB1/5DE79C05" Ref="D2"  Part="1" 
F 0 "D2" H 3792 2245 50  0000 C CNN
F 1 "LED" H 3792 2336 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 2500 50  0001 C CNN
F 3 "~" H 3800 2500 50  0001 C CNN
	1    3800 2500
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 2500 3550 2500
$Comp
L Device:LED D?
U 1 1 5DE7F022
P 3800 3075
AR Path="/5E07C1CD/5DE7F022" Ref="D?"  Part="1" 
AR Path="/5DC45DB1/5DE7F022" Ref="D3"  Part="1" 
F 0 "D3" H 3792 2820 50  0000 C CNN
F 1 "LED" H 3792 2911 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 3075 50  0001 C CNN
F 3 "~" H 3800 3075 50  0001 C CNN
	1    3800 3075
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 3075 3550 3075
$Comp
L Device:LED D?
U 1 1 5DE843A9
P 3800 3625
AR Path="/5E07C1CD/5DE843A9" Ref="D?"  Part="1" 
AR Path="/5DC45DB1/5DE843A9" Ref="D4"  Part="1" 
F 0 "D4" H 3792 3370 50  0000 C CNN
F 1 "LED" H 3792 3461 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 3625 50  0001 C CNN
F 3 "~" H 3800 3625 50  0001 C CNN
	1    3800 3625
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 3625 3550 3625
$Comp
L Device:LED D?
U 1 1 5DE89B52
P 3800 4200
AR Path="/5E07C1CD/5DE89B52" Ref="D?"  Part="1" 
AR Path="/5DC45DB1/5DE89B52" Ref="D5"  Part="1" 
F 0 "D5" H 3792 3945 50  0000 C CNN
F 1 "LED" H 3792 4036 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 4200 50  0001 C CNN
F 3 "~" H 3800 4200 50  0001 C CNN
	1    3800 4200
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 4200 3550 4200
$Comp
L Device:LED D?
U 1 1 5DE8F4C2
P 3800 4750
AR Path="/5E07C1CD/5DE8F4C2" Ref="D?"  Part="1" 
AR Path="/5DC45DB1/5DE8F4C2" Ref="D6"  Part="1" 
F 0 "D6" H 3792 4495 50  0000 C CNN
F 1 "LED" H 3792 4586 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 4750 50  0001 C CNN
F 3 "~" H 3800 4750 50  0001 C CNN
	1    3800 4750
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 4750 3550 4750
$Comp
L Device:LED D?
U 1 1 5DE94D4E
P 3800 5325
AR Path="/5E07C1CD/5DE94D4E" Ref="D?"  Part="1" 
AR Path="/5DC45DB1/5DE94D4E" Ref="D7"  Part="1" 
F 0 "D7" H 3792 5070 50  0000 C CNN
F 1 "LED" H 3792 5161 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 5325 50  0001 C CNN
F 3 "~" H 3800 5325 50  0001 C CNN
	1    3800 5325
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 5325 3550 5325
$Comp
L Device:LED D?
U 1 1 5DE9AAA3
P 3800 5875
AR Path="/5E07C1CD/5DE9AAA3" Ref="D?"  Part="1" 
AR Path="/5DC45DB1/5DE9AAA3" Ref="D8"  Part="1" 
F 0 "D8" H 3792 5620 50  0000 C CNN
F 1 "LED" H 3792 5711 50  0000 C CNN
F 2 "lib_fp:LED_0603_G" H 3800 5875 50  0001 C CNN
F 3 "~" H 3800 5875 50  0001 C CNN
	1    3800 5875
	-1   0    0    1   
$EndComp
Wire Wire Line
	3650 5875 3550 5875
Wire Wire Line
	3950 5875 4250 5875
Wire Wire Line
	3950 5325 4150 5325
Wire Wire Line
	3950 4750 4050 4750
Wire Wire Line
	3950 3075 4050 3075
Wire Wire Line
	3950 2500 4150 2500
Wire Wire Line
	3950 1950 4250 1950
$EndSCHEMATC
