EESchema Schematic File Version 4
LIBS:Control_Cerraduras-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L INA219BID:INA219BID U11
U 1 1 5E17AB09
P 6800 4075
F 0 "U11" H 6800 4845 50  0000 C CNN
F 1 "INA219BID" H 6800 4754 50  0000 C CNN
F 2 "lib_fp:SOIC-8N" H 6800 4075 50  0001 L BNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fina219" H 6800 4075 50  0001 L BNN
F 4 "IC MONITOR PWR/CURR BIDIR 8-SOIC" H 6800 4075 50  0001 L BNN "Descripcion"
F 5 "Texas Instruments" H 6800 4075 50  0001 L BNN "Fabricante"
F 6 "INA219BIDR" H 6800 4075 50  0001 L BNN "Codigo Fabricante"
F 7 "296-27899-1-ND" H 6800 4075 50  0001 L BNN "Codigo Digikey"
	1    6800 4075
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0147
U 1 1 5E17AC85
P 6050 4025
F 0 "#PWR0147" H 6050 3775 50  0001 C CNN
F 1 "GND" H 6055 3852 50  0000 C CNN
F 2 "" H 6050 4025 50  0001 C CNN
F 3 "" H 6050 4025 50  0001 C CNN
	1    6050 4025
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5E17AE29
P 6050 3525
AR Path="/5E17AE29" Ref="#PWR?"  Part="1" 
AR Path="/5DC45DB1/5E17AE29" Ref="#PWR?"  Part="1" 
AR Path="/5DCB31E2/5E17AE29" Ref="#PWR?"  Part="1" 
AR Path="/5E17A9D7/5E17AE29" Ref="#PWR0146"  Part="1" 
F 0 "#PWR0146" H 6050 3375 50  0001 C CNN
F 1 "+3.3V" H 6065 3698 50  0000 C CNN
F 2 "" H 6050 3525 50  0001 C CNN
F 3 "" H 6050 3525 50  0001 C CNN
	1    6050 3525
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3975 6050 3975
Wire Wire Line
	6050 3975 6050 4025
Wire Wire Line
	6050 3675 6100 3675
Text Notes 5375 3950 0    50   ~ 0
Address 1000000
$Comp
L power:GND #PWR0148
U 1 1 5E17B9B9
P 6050 4725
F 0 "#PWR0148" H 6050 4475 50  0001 C CNN
F 1 "GND" H 6055 4552 50  0000 C CNN
F 2 "" H 6050 4725 50  0001 C CNN
F 3 "" H 6050 4725 50  0001 C CNN
	1    6050 4725
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 4675 6050 4675
Wire Wire Line
	6050 4675 6050 4725
Wire Wire Line
	6100 3875 6050 3875
Wire Wire Line
	6050 3875 6050 3975
Connection ~ 6050 3975
Text HLabel 4550 2975 0    50   Input ~ 0
SCL
Text HLabel 4550 2875 0    50   Input ~ 0
SDA
Wire Wire Line
	6100 4175 5350 4175
Wire Wire Line
	5350 4175 5350 2975
Wire Wire Line
	7600 3675 7600 2875
Wire Wire Line
	7500 3675 7600 3675
$Comp
L Device:C C1
U 1 1 5E17CBE9
P 5850 3575
F 0 "C1" V 5598 3575 50  0000 C CNN
F 1 "0.1u" V 5689 3575 50  0000 C CNN
F 2 "lib_fp:C_0603" H 5888 3425 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0603C104Z3VACTU.pdf" H 5850 3575 50  0001 C CNN
F 4 "CAP CER 0.1UF 25V Y5V 0603" V 5850 3575 50  0001 C CNN "Descripcion"
F 5 "KEMET" V 5850 3575 50  0001 C CNN "Fabricante"
F 6 "C0603C104Z3VACTU" V 5850 3575 50  0001 C CNN "Codigo Fabricante"
F 7 "399-1100-1-ND" V 5850 3575 50  0001 C CNN "Codigo Digikey"
	1    5850 3575
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0145
U 1 1 5E17CCDB
P 5650 3625
F 0 "#PWR0145" H 5650 3375 50  0001 C CNN
F 1 "GND" H 5655 3452 50  0000 C CNN
F 2 "" H 5650 3625 50  0001 C CNN
F 3 "" H 5650 3625 50  0001 C CNN
	1    5650 3625
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3575 5650 3575
Wire Wire Line
	5650 3575 5650 3625
Wire Wire Line
	6050 3525 6050 3575
Wire Wire Line
	6050 3575 6000 3575
Connection ~ 6050 3575
Wire Wire Line
	6050 3575 6050 3675
$Comp
L Device:R R70
U 1 1 5E17E15B
P 4600 4325
F 0 "R70" V 4475 4325 50  0000 C CNN
F 1 "0.1" V 4600 4325 50  0000 C CNN
F 2 "lib_fp:R_1206" V 4530 4325 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDN0000/AOA0000C313.pdf" H 4600 4325 50  0001 C CNN
F 4 "RES 0.1 OHM 1% 1W 1206" V 4600 4325 50  0001 C CNN "Descripcion"
F 5 "Panasonic Electronic Components" V 4600 4325 50  0001 C CNN "Fabricante"
F 6 "ERJ-8BWFR100V" V 4600 4325 50  0001 C CNN "Codigo Fabricante"
F 7 "P.10AUCT-ND" V 4600 4325 50  0001 C CNN "Codigo Digikey"
	1    4600 4325
	-1   0    0    1   
$EndComp
Wire Wire Line
	6100 4475 4600 4475
Wire Wire Line
	5000 4375 6100 4375
Wire Wire Line
	4600 4175 5000 4175
Wire Wire Line
	5000 4175 5000 4375
Text HLabel 3750 4175 0    50   Input ~ 0
VIN
Text HLabel 3750 4475 0    50   Output ~ 0
VOUT
Wire Wire Line
	4600 4475 3750 4475
Connection ~ 4600 4475
Wire Wire Line
	4600 4175 3750 4175
Connection ~ 4600 4175
Wire Wire Line
	4550 2875 7600 2875
Wire Wire Line
	4550 2975 5350 2975
$EndSCHEMATC
